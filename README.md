# World Cup Diagram

This is an Eclipse Sirius extension that can be imported into Sirius and used to make multiple diagrams displaying world cup soccer information.

### Diagrams

Diagrams that can be created include:
1. Schedule of the matches (table)
2. Groups diagram
3. Country view (tree)
4. Host country with stadiums (tree)
5. Team data per group (table)
6. Player data per team (table)
7. Match data (table)
8. Rounds diagram of the final matches

## Create Your Own Diagrams

Follow these instructions to create your own soccer worldcup diagrams.

### Setup

To start working with this Sirius modeling tool, you will first need to make sure some necessarry plugins are installed. You can install them by going to **Help > Install New Software...** and using the **Photon** update site URL then select the plugins you need. At least the following plugins are required:
* Acceleo
* Acceleo Query SDK
* Sirius Properties Views - Runtime Support

Next you will have to launch a runtime *Eclipse Application*. You can do this in the **Run > Run Configurations...** by creating a new launch configuration as an **Eclipse Application**. Under the plugins section of the configurations, make sure that the `coenmv.tdt4250.worldcup.soccer.model` and `coenmv.tdt4250.worldcup.soccer.design` are selected. You can then either include all plugins from the **Target Platform** or deselect them all and click *Add Required Plug-ins*. When using the second option, make sure to check whether all necessarry plugins are selected, this should include the above listed plugins (the sirius runtime plugins might not be selected). When you are done, run the application.

After the application is done loading, go to **Window > Perspective > Open Perspective > Other...** and select the **Sirius** perspective. This should show a **Model Explorer**-window on the left. Create a new modeling project with **File > New > Modeling Project**. Then make sure to import the `coenmv.tdt4250.worldcup.soccer.design` bundle to be able to use its *ViewPoint*. This can be done through **File > Import...**. Once imported, you'll be able to create a new *Representation*. Open the view for the **representations.aird** in the modeling project. In this view, click **New...**, select the **worldcup** metamodel (**Next >**), select **WorldCup** root element (and do this same procedure for **Countries**). You can directly set the year that the worldcup takes place by going into the **Properties View** for the child node (called *World Cup*) of the **WorldCup**'s resource.

You should now have two **.worldcup** (or **.xmi**) files. Right-click the modeling project and select **Create Representation** to create a new *Representation*. Select one of the diagrams (**Next >**), and select the corresponding **.worldcup** resource. Now you can start creating your diagrams.

### Workflow

In creating the worldcup diagrams, there is a specific workflow you might want to follow.

First of all you should start out by adding some countries in a *Countries Tree*. This is easily done with the **Create Country** option and the name can directly be edited from the label.

When you have the countries you need, you should create a new *Host Tree*. This represents the hosting country where the worldcup takes place. You can create a new host with **Create Host** and select the corresponding country in the **Properties View**. This host country should have some stadiums where the soccer matches take place. Add some cities to the host and add stadiums for each of these. Again the names can be edited from the label.

The next thing that you can do is either create a *Groups Diagram* and add groups and the corresponding teams in there. Or do this in the *Team Data*. The first diagram is easier to use, but the second shows as a table and thus is better organized.

In the *Groups Diagram*, add all groups (A to H) by clicking **Initialize All Groups** in the tools window on the right, and then click anywhere in the diagram view. Use **Arange All** in the toolbar to arrange the created elements. You can then add teams to each group node by selecting **Create Team** and clicking the corresponding group. Go through the wizard that will pop-up and a team will be created (a flag might show up, but if not, there is no flag available for that country). You can drag the team to a different group if you were mistaken or delete it entirely. In the same (kind of) way you can create groups and teams in the *Team Data* representation.

You can add player and coach information to the *Player Data* table for each team. The first and last name can be edited from the label. This also holds for the shirt number, but make sure it's a positive integer. The position of a player can be given in the **Properties View**.

The next representation you want to create is the *Matches Data*. In here you can add group matches to the groups and add rounds (with their matches) that build up to the final match. When creating a match, again the wizard that pops up will help you through specifying the data. The date format is dd/MM (i.e. day and month) and the time format is hh:mm (i.e. hours and minutes). The score is formatted as {home score}-{away score}. You can only define the penalty score (for a *Round Match*) when the **Used Extra Time** flag is checked.

This way you are able to model your own worldcup diagrams. There are some more diagrams that I didn't cover here, which are:
* Rounds Tree Layout Diagram
* Matches Schedule
* (Team Data)

The data that they cover is also covered by some other diagram, but they can be used to show and/or edit data in an easier manner. The *Team Data* shows the information about the group matches that teams have played and sorts the teams in each group by the amount of points they achieved. The *Matches Schedule* shows when matches take place and in what stadium and setting a date and time for some other stadium will reschedule the match. Finally, the *Rounds Tree Layout Diagram* is used to show the matches in the rounds in a tree layout (as often is shown to the public for a worldcup). Here you can add round matches (as well as in the *Matches Data* representation) and select home and away teams. The wizard will only allow you to pick a team from the list of teams that won the previous round. The edges that show up display the score and show either blue or red depending on which team won (home or away). Try clicking **Arange All** when the tree layout is not correctly displayed.

This should help you in creating diagrams for a soccer worldcup.