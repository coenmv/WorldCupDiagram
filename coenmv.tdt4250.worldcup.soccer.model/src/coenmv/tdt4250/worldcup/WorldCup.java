/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>World Cup</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.WorldCup#getYear <em>Year</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.WorldCup#getHost <em>Host</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.WorldCup#getChampion <em>Champion</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.WorldCup#getRounds <em>Rounds</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.WorldCup#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='hostTakesPart eightGroups distinctGroupTypes containsGroupA containsGroupB containsGroupC containsGroupD containsGroupE containsGroupF containsGroupG containsGroupH eachTeamCountryUsedOnce fiveRounds distinctRoundTypes containsRoundOf16 containsQuarterFinals containsSemiFinals containsThirdPlacePlayoff containsFinal winnersRoundOf16ToQuarterFinals winnersQuarterFinalsToSemiFinals winnersSemiFinalsToFinal losersSemiFinalsToThirdPlacePlayoff best2TeamsEachGroupToRoundOf16 matchesNotAtSameTimeInSameStadium groupMatchesBeforeFinalMatches'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 hostTakesPart='self.groups-&gt;exists(group | group.teams-&gt;exists(team | team.country = self.host.country))' eightGroups='self.groups-&gt;size() = 8' distinctGroupTypes='self.groups-&gt;isUnique(group | group.groupType)' containsGroupA='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_A)' containsGroupB='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_B)' containsGroupC='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_C)' containsGroupD='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_D)' containsGroupE='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_E)' containsGroupF='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_F)' containsGroupG='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_G)' containsGroupH='self.groups-&gt;exists(group | group.groupType = worldcup::EGroupType::GROUP_H)' eachTeamCountryUsedOnce='self.groups-&gt;collect(group | group.teams)-&gt;isUnique(team | team.country)' fiveRounds='self.rounds-&gt;size() = 5' distinctRoundTypes='self.rounds-&gt;isUnique(round | round.roundType)' containsRoundOf16='self.rounds-&gt;exists(round | round.roundType = worldcup::ERoundType::ROUND_OF_16)' containsQuarterFinals='self.rounds-&gt;exists(round | round.roundType = worldcup::ERoundType::QUARTERFINALS)' containsSemiFinals='self.rounds-&gt;exists(round | round.roundType = worldcup::ERoundType::SEMIFINALS)' containsThirdPlacePlayoff='self.rounds-&gt;exists(round | round.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF)' containsFinal='self.rounds-&gt;exists(round | round.roundType = worldcup::ERoundType::FINAL)'"
 * @generated
 */
public interface WorldCup extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup_Year()
	 * @model
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.WorldCup#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Host</em>' containment reference.
	 * @see #setHost(Host)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup_Host()
	 * @model containment="true"
	 * @generated
	 */
	Host getHost();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.WorldCup#getHost <em>Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Host</em>' containment reference.
	 * @see #getHost()
	 * @generated
	 */
	void setHost(Host value);

	/**
	 * Returns the value of the '<em><b>Champion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Champion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Champion</em>' reference.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup_Champion()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Team getChampion();

	/**
	 * Returns the value of the '<em><b>Rounds</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Round}.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Round#getWorldCup <em>World Cup</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounds</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounds</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup_Rounds()
	 * @see coenmv.tdt4250.worldcup.Round#getWorldCup
	 * @model opposite="worldCup" containment="true" upper="5"
	 * @generated
	 */
	EList<Round> getRounds();

	/**
	 * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Group}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getWorldCup_Groups()
	 * @model containment="true" upper="8"
	 * @generated
	 */
	EList<Group> getGroups();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Round getRound(ERoundType roundType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Group getGroup(EGroupType groupType);

} // WorldCup
