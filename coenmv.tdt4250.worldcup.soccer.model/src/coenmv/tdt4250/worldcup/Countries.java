/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Countries</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Countries#getCountries <em>Countries</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getCountries()
 * @model
 * @generated
 */
public interface Countries extends EObject {
	/**
	 * Returns the value of the '<em><b>Countries</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Country}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Countries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Countries</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getCountries_Countries()
	 * @model containment="true"
	 * @generated
	 */
	EList<Country> getCountries();

} // Countries
