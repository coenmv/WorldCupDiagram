/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Host</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Host#getCountry <em>Country</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Host#getCities <em>Cities</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getHost()
 * @model
 * @generated
 */
public interface Host extends EObject {
	/**
	 * Returns the value of the '<em><b>Country</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Country</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Country</em>' reference.
	 * @see #setCountry(Country)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getHost_Country()
	 * @model
	 * @generated
	 */
	Country getCountry();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Host#getCountry <em>Country</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Country</em>' reference.
	 * @see #getCountry()
	 * @generated
	 */
	void setCountry(Country value);

	/**
	 * Returns the value of the '<em><b>Cities</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.City}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cities</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getHost_Cities()
	 * @model containment="true"
	 * @generated
	 */
	EList<City> getCities();

} // Host
