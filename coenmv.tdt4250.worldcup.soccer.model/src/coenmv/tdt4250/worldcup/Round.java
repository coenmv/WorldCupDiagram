/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Round</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Round#getMatches <em>Matches</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Round#getRoundType <em>Round Type</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Round#getWorldCup <em>World Cup</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getRound()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='eightMatchesRoundOf16 fourMatchesQuarterFinals twoMatchesSemiFinals oneMatchThirdPlacePlayoff oneMatchFinal'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 eightMatchesRoundOf16='if (self.roundType = worldcup::ERoundType::ROUND_OF_16) then (self.matches-&gt;size() = 8) else true endif' fourMatchesQuarterFinals='if (self.roundType = worldcup::ERoundType::QUARTERFINALS) then (self.matches-&gt;size() = 4) else true endif' twoMatchesSemiFinals='if (self.roundType = worldcup::ERoundType::SEMIFINALS) then (self.matches-&gt;size() = 2) else true endif' oneMatchThirdPlacePlayoff='if (self.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF) then (self.matches-&gt;size() = 1) else true endif' oneMatchFinal='if (self.roundType = worldcup::ERoundType::FINAL) then (self.matches-&gt;size() = 1) else true endif'"
 * @generated
 */
public interface Round extends EObject {
	/**
	 * Returns the value of the '<em><b>Matches</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.FinalMatch}.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.FinalMatch#getRound <em>Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matches</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getRound_Matches()
	 * @see coenmv.tdt4250.worldcup.FinalMatch#getRound
	 * @model opposite="round" containment="true"
	 * @generated
	 */
	EList<FinalMatch> getMatches();

	/**
	 * Returns the value of the '<em><b>Round Type</b></em>' attribute.
	 * The literals are from the enumeration {@link coenmv.tdt4250.worldcup.ERoundType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Type</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.ERoundType
	 * @see #setRoundType(ERoundType)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getRound_RoundType()
	 * @model
	 * @generated
	 */
	ERoundType getRoundType();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Round#getRoundType <em>Round Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round Type</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.ERoundType
	 * @see #getRoundType()
	 * @generated
	 */
	void setRoundType(ERoundType value);

	/**
	 * Returns the value of the '<em><b>World Cup</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.WorldCup#getRounds <em>Rounds</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>World Cup</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>World Cup</em>' container reference.
	 * @see #setWorldCup(WorldCup)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getRound_WorldCup()
	 * @see coenmv.tdt4250.worldcup.WorldCup#getRounds
	 * @model opposite="rounds" transient="false"
	 * @generated
	 */
	WorldCup getWorldCup();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Round#getWorldCup <em>World Cup</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>World Cup</em>' container reference.
	 * @see #getWorldCup()
	 * @generated
	 */
	void setWorldCup(WorldCup value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Round getNextRound();

} // Round
