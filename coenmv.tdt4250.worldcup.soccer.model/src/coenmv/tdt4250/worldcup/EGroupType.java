/**
 */
package coenmv.tdt4250.worldcup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>EGroup Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getEGroupType()
 * @model
 * @generated
 */
public enum EGroupType implements Enumerator {
	/**
	 * The '<em><b>GROUP A</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_A_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_A(0, "GROUP_A", "GROUP_A"),

	/**
	 * The '<em><b>GROUP B</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_B_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_B(1, "GROUP_B", "GROUP_B"),

	/**
	 * The '<em><b>GROUP C</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_C_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_C(2, "GROUP_C", "GROUP_C"),

	/**
	 * The '<em><b>GROUP D</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_D_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_D(3, "GROUP_D", "GROUP_D"),

	/**
	 * The '<em><b>GROUP E</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_E_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_E(4, "GROUP_E", "GROUP_E"),

	/**
	 * The '<em><b>GROUP F</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_F_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_F(5, "GROUP_F", "GROUP_F"),

	/**
	 * The '<em><b>GROUP G</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_G_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_G(6, "GROUP_G", "GROUP_G"),

	/**
	 * The '<em><b>GROUP H</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_H_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_H(7, "GROUP_H", "GROUP_H");

	/**
	 * The '<em><b>GROUP A</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP A</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_A
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_A_VALUE = 0;

	/**
	 * The '<em><b>GROUP B</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP B</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_B
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_B_VALUE = 1;

	/**
	 * The '<em><b>GROUP C</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP C</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_C
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_C_VALUE = 2;

	/**
	 * The '<em><b>GROUP D</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP D</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_D
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_D_VALUE = 3;

	/**
	 * The '<em><b>GROUP E</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP E</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_E
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_E_VALUE = 4;

	/**
	 * The '<em><b>GROUP F</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP F</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_F
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_F_VALUE = 5;

	/**
	 * The '<em><b>GROUP G</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP G</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_G
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_G_VALUE = 6;

	/**
	 * The '<em><b>GROUP H</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GROUP H</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_H
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_H_VALUE = 7;

	/**
	 * An array of all the '<em><b>EGroup Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final EGroupType[] VALUES_ARRAY =
		new EGroupType[] {
			GROUP_A,
			GROUP_B,
			GROUP_C,
			GROUP_D,
			GROUP_E,
			GROUP_F,
			GROUP_G,
			GROUP_H,
		};

	/**
	 * A public read-only list of all the '<em><b>EGroup Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<EGroupType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>EGroup Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EGroupType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EGroupType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EGroup Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EGroupType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EGroupType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EGroup Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EGroupType get(int value) {
		switch (value) {
			case GROUP_A_VALUE: return GROUP_A;
			case GROUP_B_VALUE: return GROUP_B;
			case GROUP_C_VALUE: return GROUP_C;
			case GROUP_D_VALUE: return GROUP_D;
			case GROUP_E_VALUE: return GROUP_E;
			case GROUP_F_VALUE: return GROUP_F;
			case GROUP_G_VALUE: return GROUP_G;
			case GROUP_H_VALUE: return GROUP_H;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EGroupType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //EGroupType
