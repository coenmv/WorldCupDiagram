package coenmv.tdt4250.worldcup.main;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import coenmv.tdt4250.worldcup.*;
import coenmv.tdt4250.worldcup.util.WorldCupResourceFactoryImpl;

public class ValidatorMain {

	private static void registerEMFStuff() {
		EPackage.Registry.INSTANCE.put(WorldCupPackage.eNS_PREFIX, WorldCupPackage.eINSTANCE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new WorldCupResourceFactoryImpl());
		// register AQL (an OCL implementation) constraint support
		EValidator.ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0",
				new AQLValidationDelegate());
	}

	private static String toString(WorldCup worldCup) {
		StringBuilder builder = new StringBuilder();
		builder.append("WorldCup " + worldCup.getYear() + "\n\n");
		Host host = worldCup.getHost();
		builder.append("Host: " + host.getCountry().getName() + "\n");
		for (City city : host.getCities()) {
			builder.append("\tCity: " + city.getName() + "\n");
			for (Stadium stadium : city.getStadiums()) {
				builder.append("\t\tStadium: " + stadium.getName() + "\n");
			}
		}
		builder.append("GroupStage:\n");
		for (Group group : worldCup.getGroups()) {
			builder.append("\tGroup: " + group.getGroupType() + "\n");
			for (Match match : group.getMatches()) {
				builder.append("\t\tMatch:\n");
				builder.append("\t\t\tDate: " + match.getDate() + "\n");
				builder.append("\t\t\tTime: " + match.getTime() + "\n");
				builder.append("\t\t\tStadium: " + match.getStadium().getName() + "\n");
				builder.append("\t\t\tHome: " + match.getHome().getCountry().getName() + "\n");
				builder.append("\t\t\tAway: " + match.getAway().getCountry().getName() + "\n");
				builder.append("\t\t\tScore: " + match.getScore() + "\n");
			}
			for (Team team : group.getTeams()) {
				builder.append("\t\tTeam: " + team.getCountry().getName() + "\n");
				builder.append("\t\t\tCoach: " + team.getCoach().getFullName() + "\n");
				GroupStats groupStats = team.getGroupStats();
				builder.append("\t\t\tGroupStats: " + groupStats.getMatchesPlayed() 
					+ " " + groupStats.getWon()
					+ " " + groupStats.getDraw()
					+ " " + groupStats.getLost()
					+ " " + groupStats.getGoalsFor()
					+ " " + groupStats.getGoalsAgainst()
					+ " " + groupStats.getGoalDifference()
					+ " " + groupStats.getPoints()
					+ "\n");
			}
		}
		builder.append("FinalStage:\n");
		for (Round round : worldCup.getRounds()) {
			builder.append("\tRound: " + round.getRoundType() + "\n");
			for (Match match : round.getMatches()) {
				builder.append("\t\tMatch:\n");
				builder.append("\t\t\tDate: " + match.getDate() + "\n");
				builder.append("\t\t\tTime: " + match.getTime() + "\n");
				builder.append("\t\t\tStadium: " + match.getStadium().getName() + "\n");
				builder.append("\t\t\tHome: " + match.getHome().getCountry().getName() + "\n");
				builder.append("\t\t\tAway: " + match.getAway().getCountry().getName() + "\n");
				builder.append("\t\t\tScore: " + match.getScore() + "\n");
			}
		}
		Team champion = worldCup.getChampion();
		builder.append("Champion: " + champion.getCountry().getName() + "\n");
		return builder.toString();
	}

	private static String toString(Countries countries) {
		StringBuilder builder = new StringBuilder();
		builder.append("Countries\n\n");
		for (Country country : countries.getCountries()) {
			builder.append("Country: " + country.getName() + "\n");
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		registerEMFStuff();

		ResourceSet resourceSet = new ResourceSetImpl();
		URI uri = URI.createURI(ValidatorMain.class.getResource("WorldCup.xmi").toString());
		Resource resource = resourceSet.getResource(uri, true);
		try {
			for (EObject root : resource.getContents()) {
				Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
				if (diagnostics.getSeverity() != Diagnostic.OK) {
					System.err.println(diagnostics.getMessage());
					for (Diagnostic child : diagnostics.getChildren()) {
						System.err.println(child.getMessage());
					}
				} else {
					if (root instanceof WorldCup) {
						System.out.println(toString((WorldCup) root));
					} else if (root instanceof Countries) {
						System.out.println(toString((Countries) root));
					}
				}
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

}
