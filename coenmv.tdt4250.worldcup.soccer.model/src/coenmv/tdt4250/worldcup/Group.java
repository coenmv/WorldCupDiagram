/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Group#getGroupType <em>Group Type</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Group#getMatches <em>Matches</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Group#getTeams <em>Teams</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroup()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='sixMatches fourTeams distinctTeamCountries'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 sixMatches='self.matches-&gt;size() = 6' fourTeams='self.teams-&gt;size() = 4' distinctTeamCountries='self.teams-&gt;isUnique(team | team.country)'"
 * @generated
 */
public interface Group extends EObject {
	/**
	 * Returns the value of the '<em><b>Group Type</b></em>' attribute.
	 * The literals are from the enumeration {@link coenmv.tdt4250.worldcup.EGroupType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Type</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.EGroupType
	 * @see #setGroupType(EGroupType)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroup_GroupType()
	 * @model
	 * @generated
	 */
	EGroupType getGroupType();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Group#getGroupType <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Type</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.EGroupType
	 * @see #getGroupType()
	 * @generated
	 */
	void setGroupType(EGroupType value);

	/**
	 * Returns the value of the '<em><b>Matches</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.GroupMatch}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matches</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroup_Matches()
	 * @model containment="true" upper="6"
	 * @generated
	 */
	EList<GroupMatch> getMatches();

	/**
	 * Returns the value of the '<em><b>Teams</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Team}.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Team#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Teams</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teams</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroup_Teams()
	 * @see coenmv.tdt4250.worldcup.Team#getGroup
	 * @model opposite="group" containment="true" upper="4"
	 * @generated
	 */
	EList<Team> getTeams();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Team getBestTeam();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Team getSecondBestTeam();

} // Group
