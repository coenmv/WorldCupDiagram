/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.*;

import coenmv.tdt4250.worldcup.util.Date;
import coenmv.tdt4250.worldcup.util.Score;
import coenmv.tdt4250.worldcup.util.Time;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorldCupFactoryImpl extends EFactoryImpl implements WorldCupFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorldCupFactory init() {
		try {
			WorldCupFactory theWorldCupFactory = (WorldCupFactory)EPackage.Registry.INSTANCE.getEFactory(WorldCupPackage.eNS_URI);
			if (theWorldCupFactory != null) {
				return theWorldCupFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorldCupFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldCupFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorldCupPackage.WORLD_CUP: return createWorldCup();
			case WorldCupPackage.COUNTRIES: return createCountries();
			case WorldCupPackage.COUNTRY: return createCountry();
			case WorldCupPackage.HOST: return createHost();
			case WorldCupPackage.STADIUM: return createStadium();
			case WorldCupPackage.CITY: return createCity();
			case WorldCupPackage.GROUP: return createGroup();
			case WorldCupPackage.ROUND: return createRound();
			case WorldCupPackage.GROUP_MATCH: return createGroupMatch();
			case WorldCupPackage.FINAL_MATCH: return createFinalMatch();
			case WorldCupPackage.TEAM: return createTeam();
			case WorldCupPackage.GROUP_STATS: return createGroupStats();
			case WorldCupPackage.PLAYER: return createPlayer();
			case WorldCupPackage.COACH: return createCoach();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case WorldCupPackage.EGROUP_TYPE:
				return createEGroupTypeFromString(eDataType, initialValue);
			case WorldCupPackage.EROUND_TYPE:
				return createERoundTypeFromString(eDataType, initialValue);
			case WorldCupPackage.EPOSITION:
				return createEPositionFromString(eDataType, initialValue);
			case WorldCupPackage.EDATE:
				return createEDateFromString(eDataType, initialValue);
			case WorldCupPackage.ESCORE:
				return createEScoreFromString(eDataType, initialValue);
			case WorldCupPackage.ETIME:
				return createETimeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case WorldCupPackage.EGROUP_TYPE:
				return convertEGroupTypeToString(eDataType, instanceValue);
			case WorldCupPackage.EROUND_TYPE:
				return convertERoundTypeToString(eDataType, instanceValue);
			case WorldCupPackage.EPOSITION:
				return convertEPositionToString(eDataType, instanceValue);
			case WorldCupPackage.EDATE:
				return convertEDateToString(eDataType, instanceValue);
			case WorldCupPackage.ESCORE:
				return convertEScoreToString(eDataType, instanceValue);
			case WorldCupPackage.ETIME:
				return convertETimeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldCup createWorldCup() {
		WorldCupImpl worldCup = new WorldCupImpl();
		return worldCup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Countries createCountries() {
		CountriesImpl countries = new CountriesImpl();
		return countries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Country createCountry() {
		CountryImpl country = new CountryImpl();
		return country;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Host createHost() {
		HostImpl host = new HostImpl();
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stadium createStadium() {
		StadiumImpl stadium = new StadiumImpl();
		return stadium;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public City createCity() {
		CityImpl city = new CityImpl();
		return city;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group createGroup() {
		GroupImpl group = new GroupImpl();
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Round createRound() {
		RoundImpl round = new RoundImpl();
		return round;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupMatch createGroupMatch() {
		GroupMatchImpl groupMatch = new GroupMatchImpl();
		return groupMatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FinalMatch createFinalMatch() {
		FinalMatchImpl finalMatch = new FinalMatchImpl();
		return finalMatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team createTeam() {
		TeamImpl team = new TeamImpl();
		return team;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupStats createGroupStats() {
		GroupStatsImpl groupStats = new GroupStatsImpl();
		return groupStats;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player createPlayer() {
		PlayerImpl player = new PlayerImpl();
		return player;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coach createCoach() {
		CoachImpl coach = new CoachImpl();
		return coach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EGroupType createEGroupTypeFromString(EDataType eDataType, String initialValue) {
		EGroupType result = EGroupType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEGroupTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ERoundType createERoundTypeFromString(EDataType eDataType, String initialValue) {
		ERoundType result = ERoundType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertERoundTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPosition createEPositionFromString(EDataType eDataType, String initialValue) {
		EPosition result = EPosition.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEPositionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date createEDateFromString(EDataType eDataType, String initialValue) {
		// TODO
		return Date.fromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertEDateToString(EDataType eDataType, Object instanceValue) {
		// TODO
		Date date = (Date) instanceValue;
		return date.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Score createEScoreFromString(EDataType eDataType, String initialValue) {
		// TODO
		return Score.fromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertEScoreToString(EDataType eDataType, Object instanceValue) {
		// TODO
		Score score = (Score) instanceValue;
		return score.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Time createETimeFromString(EDataType eDataType, String initialValue) {
		// TODO
		return Time.fromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertETimeToString(EDataType eDataType, Object instanceValue) {
		// TODO
		Time time = (Time) instanceValue;
		return time.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldCupPackage getWorldCupPackage() {
		return (WorldCupPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorldCupPackage getPackage() {
		return WorldCupPackage.eINSTANCE;
	}

} //WorldCupFactoryImpl
