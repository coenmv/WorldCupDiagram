/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.Match;
import coenmv.tdt4250.worldcup.Stadium;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import coenmv.tdt4250.worldcup.util.Date;
import coenmv.tdt4250.worldcup.util.Score;
import coenmv.tdt4250.worldcup.util.Time;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Match</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getStadium <em>Stadium</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getHome <em>Home</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getAway <em>Away</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getScore <em>Score</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getTime <em>Time</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.MatchImpl#getDate <em>Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MatchImpl extends MinimalEObjectImpl.Container implements Match {
	/**
	 * The cached value of the '{@link #getStadium() <em>Stadium</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStadium()
	 * @generated
	 * @ordered
	 */
	protected Stadium stadium;

	/**
	 * The cached value of the '{@link #getHome() <em>Home</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHome()
	 * @generated
	 * @ordered
	 */
	protected Team home;

	/**
	 * The cached value of the '{@link #getAway() <em>Away</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAway()
	 * @generated
	 * @ordered
	 */
	protected Team away;

	/**
	 * The default value of the '{@link #getScore() <em>Score</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getScore()
	 * @generated
	 * @ordered
	 */
	protected static final Score SCORE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScore() <em>Score</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getScore()
	 * @generated
	 * @ordered
	 */
	protected Score score = SCORE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final Time TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected Time time = TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected Date date = DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.MATCH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Stadium getStadium() {
		if (stadium != null && stadium.eIsProxy()) {
			InternalEObject oldStadium = (InternalEObject)stadium;
			stadium = (Stadium)eResolveProxy(oldStadium);
			if (stadium != oldStadium) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorldCupPackage.MATCH__STADIUM, oldStadium, stadium));
			}
		}
		return stadium;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Stadium basicGetStadium() {
		return stadium;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setStadium(Stadium newStadium) {
		Stadium oldStadium = stadium;
		stadium = newStadium;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__STADIUM, oldStadium, stadium));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Team getHome() {
		if (home != null && home.eIsProxy()) {
			InternalEObject oldHome = (InternalEObject)home;
			home = (Team)eResolveProxy(oldHome);
			if (home != oldHome) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorldCupPackage.MATCH__HOME, oldHome, home));
			}
		}
		return home;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetHome() {
		return home;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setHome(Team newHome) {
		Team oldHome = home;
		home = newHome;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__HOME, oldHome, home));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Team getAway() {
		if (away != null && away.eIsProxy()) {
			InternalEObject oldAway = (InternalEObject)away;
			away = (Team)eResolveProxy(oldAway);
			if (away != oldAway) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorldCupPackage.MATCH__AWAY, oldAway, away));
			}
		}
		return away;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetAway() {
		return away;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAway(Team newAway) {
		Team oldAway = away;
		away = newAway;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__AWAY, oldAway, away));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Score getScore() {
		return score;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setScore(Score newScore) {
		Score oldScore = score;
		score = newScore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__SCORE, oldScore, score));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Time getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(Time newTime) {
		Time oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(Date newDate) {
		Date oldDate = date;
		date = newDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.MATCH__DATE, oldDate, date));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Team getWinner() {
		// TODO
		if (getScore() != null) {
			int homeScore = getScore().homeScore;
			int awayScore = getScore().awayScore;
			if (homeScore < awayScore) {
				return getAway();
			} else if (homeScore > awayScore) {
				return getHome();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Team getLoser() {
		// TODO
		if (getScore() != null) {
			int homeScore = getScore().homeScore;
			int awayScore = getScore().awayScore;
			if (homeScore < awayScore) {
				return getHome();
			} else if (homeScore > awayScore) {
				return getAway();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isDraw() {
		// TODO
		if (getScore() == null) {
			return false;
		}
		int homeScore = getScore().homeScore;
		int awayScore = getScore().awayScore;
		if (homeScore == awayScore) {
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.MATCH__STADIUM:
				if (resolve) return getStadium();
				return basicGetStadium();
			case WorldCupPackage.MATCH__HOME:
				if (resolve) return getHome();
				return basicGetHome();
			case WorldCupPackage.MATCH__AWAY:
				if (resolve) return getAway();
				return basicGetAway();
			case WorldCupPackage.MATCH__SCORE:
				return getScore();
			case WorldCupPackage.MATCH__TIME:
				return getTime();
			case WorldCupPackage.MATCH__DATE:
				return getDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.MATCH__STADIUM:
				setStadium((Stadium)newValue);
				return;
			case WorldCupPackage.MATCH__HOME:
				setHome((Team)newValue);
				return;
			case WorldCupPackage.MATCH__AWAY:
				setAway((Team)newValue);
				return;
			case WorldCupPackage.MATCH__SCORE:
				setScore((Score)newValue);
				return;
			case WorldCupPackage.MATCH__TIME:
				setTime((Time)newValue);
				return;
			case WorldCupPackage.MATCH__DATE:
				setDate((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.MATCH__STADIUM:
				setStadium((Stadium)null);
				return;
			case WorldCupPackage.MATCH__HOME:
				setHome((Team)null);
				return;
			case WorldCupPackage.MATCH__AWAY:
				setAway((Team)null);
				return;
			case WorldCupPackage.MATCH__SCORE:
				setScore(SCORE_EDEFAULT);
				return;
			case WorldCupPackage.MATCH__TIME:
				setTime(TIME_EDEFAULT);
				return;
			case WorldCupPackage.MATCH__DATE:
				setDate(DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.MATCH__STADIUM:
				return stadium != null;
			case WorldCupPackage.MATCH__HOME:
				return home != null;
			case WorldCupPackage.MATCH__AWAY:
				return away != null;
			case WorldCupPackage.MATCH__SCORE:
				return SCORE_EDEFAULT == null ? score != null : !SCORE_EDEFAULT.equals(score);
			case WorldCupPackage.MATCH__TIME:
				return TIME_EDEFAULT == null ? time != null : !TIME_EDEFAULT.equals(time);
			case WorldCupPackage.MATCH__DATE:
				return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldCupPackage.MATCH___GET_WINNER:
				return getWinner();
			case WorldCupPackage.MATCH___GET_LOSER:
				return getLoser();
			case WorldCupPackage.MATCH___IS_DRAW:
				return isDraw();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (score: ");
		result.append(score);
		result.append(", time: ");
		result.append(time);
		result.append(", date: ");
		result.append(date);
		result.append(')');
		return result.toString();
	}

} // MatchImpl
