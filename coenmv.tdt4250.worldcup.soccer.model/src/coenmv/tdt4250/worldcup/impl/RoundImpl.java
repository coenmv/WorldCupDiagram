/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.ERoundType;
import coenmv.tdt4250.worldcup.FinalMatch;
import coenmv.tdt4250.worldcup.Round;
import coenmv.tdt4250.worldcup.WorldCup;
import coenmv.tdt4250.worldcup.WorldCupPackage;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Round</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.RoundImpl#getMatches <em>Matches</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.RoundImpl#getRoundType <em>Round Type</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.RoundImpl#getWorldCup <em>World Cup</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoundImpl extends MinimalEObjectImpl.Container implements Round {
	/**
	 * The cached value of the '{@link #getMatches() <em>Matches</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMatches()
	 * @generated
	 * @ordered
	 */
	protected EList<FinalMatch> matches;

	/**
	 * The default value of the '{@link #getRoundType() <em>Round Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRoundType()
	 * @generated
	 * @ordered
	 */
	protected static final ERoundType ROUND_TYPE_EDEFAULT = ERoundType.ROUND_OF_16;

	/**
	 * The cached value of the '{@link #getRoundType() <em>Round Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRoundType()
	 * @generated
	 * @ordered
	 */
	protected ERoundType roundType = ROUND_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected RoundImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.ROUND;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FinalMatch> getMatches() {
		if (matches == null) {
			matches = new EObjectContainmentWithInverseEList<FinalMatch>(FinalMatch.class, this, WorldCupPackage.ROUND__MATCHES, WorldCupPackage.FINAL_MATCH__ROUND);
		}
		return matches;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ERoundType getRoundType() {
		return roundType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundType(ERoundType newRoundType) {
		ERoundType oldRoundType = roundType;
		roundType = newRoundType == null ? ROUND_TYPE_EDEFAULT : newRoundType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.ROUND__ROUND_TYPE, oldRoundType, roundType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorldCup getWorldCup() {
		if (eContainerFeatureID() != WorldCupPackage.ROUND__WORLD_CUP) return null;
		return (WorldCup)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorldCup(WorldCup newWorldCup, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newWorldCup, WorldCupPackage.ROUND__WORLD_CUP, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorldCup(WorldCup newWorldCup) {
		if (newWorldCup != eInternalContainer() || (eContainerFeatureID() != WorldCupPackage.ROUND__WORLD_CUP && newWorldCup != null)) {
			if (EcoreUtil.isAncestor(this, newWorldCup))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newWorldCup != null)
				msgs = ((InternalEObject)newWorldCup).eInverseAdd(this, WorldCupPackage.WORLD_CUP__ROUNDS, WorldCup.class, msgs);
			msgs = basicSetWorldCup(newWorldCup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.ROUND__WORLD_CUP, newWorldCup, newWorldCup));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Round getNextRound() {
		// TODO
		ERoundType nextRoundType = ERoundType.ROUND_OF_16;
		switch (getRoundType()) {
		case ROUND_OF_16:
			nextRoundType = ERoundType.QUARTERFINALS;
			break;
		case QUARTERFINALS:
			nextRoundType = ERoundType.SEMIFINALS;
			break;
		case SEMIFINALS:
			nextRoundType = ERoundType.FINAL;
			break;
		default:
			return null;		
		}
		return getWorldCup().getRound(nextRoundType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMatches()).basicAdd(otherEnd, msgs);
			case WorldCupPackage.ROUND__WORLD_CUP:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetWorldCup((WorldCup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				return ((InternalEList<?>)getMatches()).basicRemove(otherEnd, msgs);
			case WorldCupPackage.ROUND__WORLD_CUP:
				return basicSetWorldCup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WorldCupPackage.ROUND__WORLD_CUP:
				return eInternalContainer().eInverseRemove(this, WorldCupPackage.WORLD_CUP__ROUNDS, WorldCup.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				return getMatches();
			case WorldCupPackage.ROUND__ROUND_TYPE:
				return getRoundType();
			case WorldCupPackage.ROUND__WORLD_CUP:
				return getWorldCup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				getMatches().clear();
				getMatches().addAll((Collection<? extends FinalMatch>)newValue);
				return;
			case WorldCupPackage.ROUND__ROUND_TYPE:
				setRoundType((ERoundType)newValue);
				return;
			case WorldCupPackage.ROUND__WORLD_CUP:
				setWorldCup((WorldCup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				getMatches().clear();
				return;
			case WorldCupPackage.ROUND__ROUND_TYPE:
				setRoundType(ROUND_TYPE_EDEFAULT);
				return;
			case WorldCupPackage.ROUND__WORLD_CUP:
				setWorldCup((WorldCup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.ROUND__MATCHES:
				return matches != null && !matches.isEmpty();
			case WorldCupPackage.ROUND__ROUND_TYPE:
				return roundType != ROUND_TYPE_EDEFAULT;
			case WorldCupPackage.ROUND__WORLD_CUP:
				return getWorldCup() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldCupPackage.ROUND___GET_NEXT_ROUND:
				return getNextRound();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (roundType: ");
		result.append(roundType);
		result.append(')');
		return result.toString();
	}

} // RoundImpl
