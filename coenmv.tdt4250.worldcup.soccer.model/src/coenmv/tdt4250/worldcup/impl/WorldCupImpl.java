/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.EGroupType;
import coenmv.tdt4250.worldcup.ERoundType;
import coenmv.tdt4250.worldcup.Group;
import coenmv.tdt4250.worldcup.Host;
import coenmv.tdt4250.worldcup.Match;
import coenmv.tdt4250.worldcup.Round;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCup;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>World Cup</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl#getYear <em>Year</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl#getHost <em>Host</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl#getChampion <em>Champion</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl#getRounds <em>Rounds</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorldCupImpl extends MinimalEObjectImpl.Container implements WorldCup {
	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHost() <em>Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected Host host;

	/**
	 * The cached value of the '{@link #getRounds() <em>Rounds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRounds()
	 * @generated
	 * @ordered
	 */
	protected EList<Round> rounds;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<Group> groups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorldCupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.WORLD_CUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.WORLD_CUP__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Host getHost() {
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHost(Host newHost, NotificationChain msgs) {
		Host oldHost = host;
		host = newHost;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorldCupPackage.WORLD_CUP__HOST, oldHost, newHost);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHost(Host newHost) {
		if (newHost != host) {
			NotificationChain msgs = null;
			if (host != null)
				msgs = ((InternalEObject)host).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorldCupPackage.WORLD_CUP__HOST, null, msgs);
			if (newHost != null)
				msgs = ((InternalEObject)newHost).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WorldCupPackage.WORLD_CUP__HOST, null, msgs);
			msgs = basicSetHost(newHost, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.WORLD_CUP__HOST, newHost, newHost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team getChampion() {
		Team champion = basicGetChampion();
		return champion != null && champion.eIsProxy() ? (Team)eResolveProxy((InternalEObject)champion) : champion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Team basicGetChampion() {
		// TODO
		Round finalRound = getRound(ERoundType.FINAL);
		if (finalRound == null) {
			return null;
		}
		Match finalMatch = finalRound.getMatches().get(0);
		return finalMatch.getWinner();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Round> getRounds() {
		if (rounds == null) {
			rounds = new EObjectContainmentWithInverseEList<Round>(Round.class, this, WorldCupPackage.WORLD_CUP__ROUNDS, WorldCupPackage.ROUND__WORLD_CUP);
		}
		return rounds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Group> getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentEList<Group>(Group.class, this, WorldCupPackage.WORLD_CUP__GROUPS);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Round getRound(ERoundType roundType) {
		// TODO
		for (Round round : getRounds()) {
			if (round.getRoundType() == roundType) {
				return round;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Group getGroup(EGroupType groupType) {
		// TODO
		for (Group group : getGroups()) {
			if (group.getGroupType() == groupType) {
				return group;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRounds()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__HOST:
				return basicSetHost(null, msgs);
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				return ((InternalEList<?>)getRounds()).basicRemove(otherEnd, msgs);
			case WorldCupPackage.WORLD_CUP__GROUPS:
				return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__YEAR:
				return getYear();
			case WorldCupPackage.WORLD_CUP__HOST:
				return getHost();
			case WorldCupPackage.WORLD_CUP__CHAMPION:
				if (resolve) return getChampion();
				return basicGetChampion();
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				return getRounds();
			case WorldCupPackage.WORLD_CUP__GROUPS:
				return getGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__YEAR:
				setYear((Integer)newValue);
				return;
			case WorldCupPackage.WORLD_CUP__HOST:
				setHost((Host)newValue);
				return;
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				getRounds().clear();
				getRounds().addAll((Collection<? extends Round>)newValue);
				return;
			case WorldCupPackage.WORLD_CUP__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection<? extends Group>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case WorldCupPackage.WORLD_CUP__HOST:
				setHost((Host)null);
				return;
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				getRounds().clear();
				return;
			case WorldCupPackage.WORLD_CUP__GROUPS:
				getGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.WORLD_CUP__YEAR:
				return year != YEAR_EDEFAULT;
			case WorldCupPackage.WORLD_CUP__HOST:
				return host != null;
			case WorldCupPackage.WORLD_CUP__CHAMPION:
				return basicGetChampion() != null;
			case WorldCupPackage.WORLD_CUP__ROUNDS:
				return rounds != null && !rounds.isEmpty();
			case WorldCupPackage.WORLD_CUP__GROUPS:
				return groups != null && !groups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldCupPackage.WORLD_CUP___GET_ROUND__EROUNDTYPE:
				return getRound((ERoundType)arguments.get(0));
			case WorldCupPackage.WORLD_CUP___GET_GROUP__EGROUPTYPE:
				return getGroup((EGroupType)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (year: ");
		result.append(year);
		result.append(')');
		return result.toString();
	}

} //WorldCupImpl
