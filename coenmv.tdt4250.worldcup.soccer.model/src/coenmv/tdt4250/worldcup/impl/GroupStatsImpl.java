/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.Group;

import coenmv.tdt4250.worldcup.GroupStats;
import coenmv.tdt4250.worldcup.Match;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import static coenmv.tdt4250.worldcup.util.Util.equalsIgnoreNull;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Group
 * Stats</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getTeam <em>Team</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getMatchesPlayed <em>Matches Played</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getWon <em>Won</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getDraw <em>Draw</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getLost <em>Lost</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getGoalsFor <em>Goals For</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getGoalsAgainst <em>Goals Against</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getGoalDifference <em>Goal Difference</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroupStatsImpl extends MinimalEObjectImpl.Container implements GroupStats {
	/**
	 * The default value of the '{@link #getMatchesPlayed() <em>Matches Played</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMatchesPlayed()
	 * @generated
	 * @ordered
	 */
	protected static final int MATCHES_PLAYED_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getWon() <em>Won</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWon()
	 * @generated
	 * @ordered
	 */
	protected static final int WON_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getDraw() <em>Draw</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDraw()
	 * @generated
	 * @ordered
	 */
	protected static final int DRAW_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getLost() <em>Lost</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLost()
	 * @generated
	 * @ordered
	 */
	protected static final int LOST_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getGoalsFor() <em>Goals For</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGoalsFor()
	 * @generated
	 * @ordered
	 */
	protected static final int GOALS_FOR_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getGoalsAgainst() <em>Goals Against</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGoalsAgainst()
	 * @generated
	 * @ordered
	 */
	protected static final int GOALS_AGAINST_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getGoalDifference() <em>Goal Difference</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGoalDifference()
	 * @generated
	 * @ordered
	 */
	protected static final int GOAL_DIFFERENCE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getPoints() <em>Points</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPoints()
	 * @generated
	 * @ordered
	 */
	protected static final int POINTS_EDEFAULT = 0;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupStatsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.GROUP_STATS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Team getTeam() {
		if (eContainerFeatureID() != WorldCupPackage.GROUP_STATS__TEAM) return null;
		return (Team)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTeam(Team newTeam, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTeam, WorldCupPackage.GROUP_STATS__TEAM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTeam(Team newTeam) {
		if (newTeam != eInternalContainer() || (eContainerFeatureID() != WorldCupPackage.GROUP_STATS__TEAM && newTeam != null)) {
			if (EcoreUtil.isAncestor(this, newTeam))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTeam != null)
				msgs = ((InternalEObject)newTeam).eInverseAdd(this, WorldCupPackage.TEAM__GROUP_STATS, Team.class, msgs);
			msgs = basicSetTeam(newTeam, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.GROUP_STATS__TEAM, newTeam, newTeam));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getMatchesPlayed() {
		// TODO: maybe change to won+lost+draw
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome()) || equalsIgnoreNull(team, match.getAway())) {
				count++;
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getWon() {
		// TODO
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			Team winner = match.getWinner();
			if (equalsIgnoreNull(winner, team)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getDraw() {
		// TODO
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome()) || equalsIgnoreNull(team, match.getAway())) {
				if (match.isDraw()) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getLost() {
		// TODO
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			Team loser = match.getLoser();
			if (equalsIgnoreNull(loser, team)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getGoalsFor() {
		// TODO
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome())) {
				if (match.getScore() != null) {
					count += match.getScore().homeScore;
				}
			} else if (equalsIgnoreNull(team, match.getAway())) {
				if (match.getScore() != null) {
					count += match.getScore().awayScore;
				}
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getGoalsAgainst() {
		// TODO
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome())) {
				if (match.getScore() != null) {
					count += match.getScore().awayScore;
				}
			} else if (equalsIgnoreNull(team, match.getAway())) {
				if (match.getScore() != null) {
					count += match.getScore().homeScore;
				}
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getGoalDifference() {
		// TODO: maybe change to goalsFor-goalsAgainst
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome())) {
				if (match.getScore() != null) {
					count += match.getScore().homeScore - match.getScore().awayScore;
				}
			} else if (equalsIgnoreNull(team, match.getAway())) {
				if (match.getScore() != null) {
					count += match.getScore().awayScore - match.getScore().homeScore;
				}
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getPoints() {
		// TODO: maybe change to won*3+draw
		Team team = getTeam();
		Group group = team.getGroup();
		int count = 0;
		for (Match match : group.getMatches()) {
			if (equalsIgnoreNull(team, match.getHome()) || equalsIgnoreNull(team, match.getAway())) {
				Team winner = match.getWinner();
				if (equalsIgnoreNull(team, winner)) {
					count += 3;
				} else if (match.isDraw()) {
					count += 1;
				}
			}
		}
		return count;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTeam((Team)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				return basicSetTeam(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				return eInternalContainer().eInverseRemove(this, WorldCupPackage.TEAM__GROUP_STATS, Team.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				return getTeam();
			case WorldCupPackage.GROUP_STATS__MATCHES_PLAYED:
				return getMatchesPlayed();
			case WorldCupPackage.GROUP_STATS__WON:
				return getWon();
			case WorldCupPackage.GROUP_STATS__DRAW:
				return getDraw();
			case WorldCupPackage.GROUP_STATS__LOST:
				return getLost();
			case WorldCupPackage.GROUP_STATS__GOALS_FOR:
				return getGoalsFor();
			case WorldCupPackage.GROUP_STATS__GOALS_AGAINST:
				return getGoalsAgainst();
			case WorldCupPackage.GROUP_STATS__GOAL_DIFFERENCE:
				return getGoalDifference();
			case WorldCupPackage.GROUP_STATS__POINTS:
				return getPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				setTeam((Team)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				setTeam((Team)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.GROUP_STATS__TEAM:
				return getTeam() != null;
			case WorldCupPackage.GROUP_STATS__MATCHES_PLAYED:
				return getMatchesPlayed() != MATCHES_PLAYED_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__WON:
				return getWon() != WON_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__DRAW:
				return getDraw() != DRAW_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__LOST:
				return getLost() != LOST_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__GOALS_FOR:
				return getGoalsFor() != GOALS_FOR_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__GOALS_AGAINST:
				return getGoalsAgainst() != GOALS_AGAINST_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__GOAL_DIFFERENCE:
				return getGoalDifference() != GOAL_DIFFERENCE_EDEFAULT;
			case WorldCupPackage.GROUP_STATS__POINTS:
				return getPoints() != POINTS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} // GroupStatsImpl
