/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.Coach;
import coenmv.tdt4250.worldcup.Country;
import coenmv.tdt4250.worldcup.Group;
import coenmv.tdt4250.worldcup.GroupStats;
import coenmv.tdt4250.worldcup.Player;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCupPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Team</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.TeamImpl#getCoach <em>Coach</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.TeamImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.TeamImpl#getGroupStats <em>Group Stats</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.TeamImpl#getCountry <em>Country</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.TeamImpl#getPlayers <em>Players</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TeamImpl extends MinimalEObjectImpl.Container implements Team {
	/**
	 * The cached value of the '{@link #getCoach() <em>Coach</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoach()
	 * @generated
	 * @ordered
	 */
	protected Coach coach;

	/**
	 * The cached value of the '{@link #getGroupStats() <em>Group Stats</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupStats()
	 * @generated
	 * @ordered
	 */
	protected GroupStats groupStats;

	/**
	 * The cached value of the '{@link #getCountry() <em>Country</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountry()
	 * @generated
	 * @ordered
	 */
	protected Country country;

	/**
	 * The cached value of the '{@link #getPlayers() <em>Players</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayers()
	 * @generated
	 * @ordered
	 */
	protected EList<Player> players;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TeamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.TEAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coach getCoach() {
		return coach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoach(Coach newCoach, NotificationChain msgs) {
		Coach oldCoach = coach;
		coach = newCoach;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__COACH, oldCoach, newCoach);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoach(Coach newCoach) {
		if (newCoach != coach) {
			NotificationChain msgs = null;
			if (coach != null)
				msgs = ((InternalEObject)coach).eInverseRemove(this, WorldCupPackage.COACH__TEAM, Coach.class, msgs);
			if (newCoach != null)
				msgs = ((InternalEObject)newCoach).eInverseAdd(this, WorldCupPackage.COACH__TEAM, Coach.class, msgs);
			msgs = basicSetCoach(newCoach, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__COACH, newCoach, newCoach));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group getGroup() {
		if (eContainerFeatureID() != WorldCupPackage.TEAM__GROUP) return null;
		return (Group)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroup(Group newGroup, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGroup, WorldCupPackage.TEAM__GROUP, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(Group newGroup) {
		if (newGroup != eInternalContainer() || (eContainerFeatureID() != WorldCupPackage.TEAM__GROUP && newGroup != null)) {
			if (EcoreUtil.isAncestor(this, newGroup))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGroup != null)
				msgs = ((InternalEObject)newGroup).eInverseAdd(this, WorldCupPackage.GROUP__TEAMS, Group.class, msgs);
			msgs = basicSetGroup(newGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__GROUP, newGroup, newGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupStats getGroupStats() {
		return groupStats;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroupStats(GroupStats newGroupStats, NotificationChain msgs) {
		GroupStats oldGroupStats = groupStats;
		groupStats = newGroupStats;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__GROUP_STATS, oldGroupStats, newGroupStats);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupStats(GroupStats newGroupStats) {
		if (newGroupStats != groupStats) {
			NotificationChain msgs = null;
			if (groupStats != null)
				msgs = ((InternalEObject)groupStats).eInverseRemove(this, WorldCupPackage.GROUP_STATS__TEAM, GroupStats.class, msgs);
			if (newGroupStats != null)
				msgs = ((InternalEObject)newGroupStats).eInverseAdd(this, WorldCupPackage.GROUP_STATS__TEAM, GroupStats.class, msgs);
			msgs = basicSetGroupStats(newGroupStats, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__GROUP_STATS, newGroupStats, newGroupStats));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Country getCountry() {
		if (country != null && country.eIsProxy()) {
			InternalEObject oldCountry = (InternalEObject)country;
			country = (Country)eResolveProxy(oldCountry);
			if (country != oldCountry) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorldCupPackage.TEAM__COUNTRY, oldCountry, country));
			}
		}
		return country;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Country basicGetCountry() {
		return country;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountry(Country newCountry) {
		Country oldCountry = country;
		country = newCountry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.TEAM__COUNTRY, oldCountry, country));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Player> getPlayers() {
		if (players == null) {
			players = new EObjectContainmentEList<Player>(Player.class, this, WorldCupPackage.TEAM__PLAYERS);
		}
		return players;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				if (coach != null)
					msgs = ((InternalEObject)coach).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorldCupPackage.TEAM__COACH, null, msgs);
				return basicSetCoach((Coach)otherEnd, msgs);
			case WorldCupPackage.TEAM__GROUP:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGroup((Group)otherEnd, msgs);
			case WorldCupPackage.TEAM__GROUP_STATS:
				if (groupStats != null)
					msgs = ((InternalEObject)groupStats).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorldCupPackage.TEAM__GROUP_STATS, null, msgs);
				return basicSetGroupStats((GroupStats)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				return basicSetCoach(null, msgs);
			case WorldCupPackage.TEAM__GROUP:
				return basicSetGroup(null, msgs);
			case WorldCupPackage.TEAM__GROUP_STATS:
				return basicSetGroupStats(null, msgs);
			case WorldCupPackage.TEAM__PLAYERS:
				return ((InternalEList<?>)getPlayers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WorldCupPackage.TEAM__GROUP:
				return eInternalContainer().eInverseRemove(this, WorldCupPackage.GROUP__TEAMS, Group.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				return getCoach();
			case WorldCupPackage.TEAM__GROUP:
				return getGroup();
			case WorldCupPackage.TEAM__GROUP_STATS:
				return getGroupStats();
			case WorldCupPackage.TEAM__COUNTRY:
				if (resolve) return getCountry();
				return basicGetCountry();
			case WorldCupPackage.TEAM__PLAYERS:
				return getPlayers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				setCoach((Coach)newValue);
				return;
			case WorldCupPackage.TEAM__GROUP:
				setGroup((Group)newValue);
				return;
			case WorldCupPackage.TEAM__GROUP_STATS:
				setGroupStats((GroupStats)newValue);
				return;
			case WorldCupPackage.TEAM__COUNTRY:
				setCountry((Country)newValue);
				return;
			case WorldCupPackage.TEAM__PLAYERS:
				getPlayers().clear();
				getPlayers().addAll((Collection<? extends Player>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				setCoach((Coach)null);
				return;
			case WorldCupPackage.TEAM__GROUP:
				setGroup((Group)null);
				return;
			case WorldCupPackage.TEAM__GROUP_STATS:
				setGroupStats((GroupStats)null);
				return;
			case WorldCupPackage.TEAM__COUNTRY:
				setCountry((Country)null);
				return;
			case WorldCupPackage.TEAM__PLAYERS:
				getPlayers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.TEAM__COACH:
				return coach != null;
			case WorldCupPackage.TEAM__GROUP:
				return getGroup() != null;
			case WorldCupPackage.TEAM__GROUP_STATS:
				return groupStats != null;
			case WorldCupPackage.TEAM__COUNTRY:
				return country != null;
			case WorldCupPackage.TEAM__PLAYERS:
				return players != null && !players.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TeamImpl
