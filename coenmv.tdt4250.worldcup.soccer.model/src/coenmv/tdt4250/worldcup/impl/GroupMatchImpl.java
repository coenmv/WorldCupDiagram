/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.GroupMatch;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GroupMatchImpl extends MatchImpl implements GroupMatch {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupMatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.GROUP_MATCH;
	}

} //GroupMatchImpl
