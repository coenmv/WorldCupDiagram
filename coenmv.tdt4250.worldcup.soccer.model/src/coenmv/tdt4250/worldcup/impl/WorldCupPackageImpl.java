/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.City;
import coenmv.tdt4250.worldcup.Coach;
import coenmv.tdt4250.worldcup.Countries;
import coenmv.tdt4250.worldcup.Country;
import coenmv.tdt4250.worldcup.EGroupType;
import coenmv.tdt4250.worldcup.EPosition;
import coenmv.tdt4250.worldcup.ERoundType;
import coenmv.tdt4250.worldcup.FinalMatch;
import coenmv.tdt4250.worldcup.Group;
import coenmv.tdt4250.worldcup.GroupMatch;
import coenmv.tdt4250.worldcup.GroupStats;
import coenmv.tdt4250.worldcup.Host;
import coenmv.tdt4250.worldcup.Match;
import coenmv.tdt4250.worldcup.Person;
import coenmv.tdt4250.worldcup.Player;
import coenmv.tdt4250.worldcup.Round;
import coenmv.tdt4250.worldcup.Stadium;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCup;
import coenmv.tdt4250.worldcup.WorldCupFactory;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import coenmv.tdt4250.worldcup.util.Date;
import coenmv.tdt4250.worldcup.util.Score;
import coenmv.tdt4250.worldcup.util.Time;

import coenmv.tdt4250.worldcup.util.WorldCupValidator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class WorldCupPackageImpl extends EPackageImpl implements WorldCupPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldCupEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass countriesEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass countryEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hostEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stadiumEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cityEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roundEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupMatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass finalMatchEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teamEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupStatsEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass playerEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coachEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eGroupTypeEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eRoundTypeEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ePositionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eDateEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eScoreEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eTimeEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorldCupPackageImpl() {
		super(eNS_URI, WorldCupFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorldCupPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorldCupPackage init() {
		if (isInited) return (WorldCupPackage)EPackage.Registry.INSTANCE.getEPackage(WorldCupPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorldCupPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorldCupPackageImpl theWorldCupPackage = registeredWorldCupPackage instanceof WorldCupPackageImpl ? (WorldCupPackageImpl)registeredWorldCupPackage : new WorldCupPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theWorldCupPackage.createPackageContents();

		// Initialize created meta-data
		theWorldCupPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theWorldCupPackage,
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return WorldCupValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theWorldCupPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorldCupPackage.eNS_URI, theWorldCupPackage);
		return theWorldCupPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorldCup() {
		return worldCupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWorldCup_Year() {
		return (EAttribute)worldCupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldCup_Host() {
		return (EReference)worldCupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldCup_Champion() {
		return (EReference)worldCupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldCup_Rounds() {
		return (EReference)worldCupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldCup_Groups() {
		return (EReference)worldCupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getWorldCup__GetRound__ERoundType() {
		return worldCupEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getWorldCup__GetGroup__EGroupType() {
		return worldCupEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCountries() {
		return countriesEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCountries_Countries() {
		return (EReference)countriesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCountry() {
		return countryEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCountry_Name() {
		return (EAttribute)countryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHost() {
		return hostEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHost_Country() {
		return (EReference)hostEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHost_Cities() {
		return (EReference)hostEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStadium() {
		return stadiumEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStadium_Name() {
		return (EAttribute)stadiumEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCity() {
		return cityEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCity_Name() {
		return (EAttribute)cityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCity_Stadiums() {
		return (EReference)cityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroup() {
		return groupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroup_GroupType() {
		return (EAttribute)groupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroup_Matches() {
		return (EReference)groupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroup_Teams() {
		return (EReference)groupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroup__GetBestTeam() {
		return groupEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroup__GetSecondBestTeam() {
		return groupEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRound() {
		return roundEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRound_Matches() {
		return (EReference)roundEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRound_RoundType() {
		return (EAttribute)roundEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRound_WorldCup() {
		return (EReference)roundEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRound__GetNextRound() {
		return roundEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMatch() {
		return matchEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_Stadium() {
		return (EReference)matchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_Home() {
		return (EReference)matchEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_Away() {
		return (EReference)matchEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMatch_Score() {
		return (EAttribute)matchEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMatch_Time() {
		return (EAttribute)matchEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMatch_Date() {
		return (EAttribute)matchEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMatch__GetWinner() {
		return matchEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMatch__GetLoser() {
		return matchEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMatch__IsDraw() {
		return matchEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroupMatch() {
		return groupMatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFinalMatch() {
		return finalMatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFinalMatch_UsedExtraTime() {
		return (EAttribute)finalMatchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFinalMatch_UsedPenalties() {
		return (EAttribute)finalMatchEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFinalMatch_PenaltyScore() {
		return (EAttribute)finalMatchEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFinalMatch_Round() {
		return (EReference)finalMatchEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFinalMatch__GetFollowingMatch() {
		return finalMatchEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTeam() {
		return teamEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTeam_Coach() {
		return (EReference)teamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTeam_Group() {
		return (EReference)teamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTeam_GroupStats() {
		return (EReference)teamEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTeam_Country() {
		return (EReference)teamEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTeam_Players() {
		return (EReference)teamEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroupStats() {
		return groupStatsEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroupStats_Team() {
		return (EReference)groupStatsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_MatchesPlayed() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_Won() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_Draw() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_Lost() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_GoalsFor() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_GoalsAgainst() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_GoalDifference() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupStats_Points() {
		return (EAttribute)groupStatsEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPlayer() {
		return playerEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlayer_Position() {
		return (EAttribute)playerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlayer_ShirtNumber() {
		return (EAttribute)playerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoach() {
		return coachEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoach_Team() {
		return (EReference)coachEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerson() {
		return personEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_FirstName() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_Surname() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_FullName() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEGroupType() {
		return eGroupTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getERoundType() {
		return eRoundTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEPosition() {
		return ePositionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEDate() {
		return eDateEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEScore() {
		return eScoreEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getETime() {
		return eTimeEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorldCupFactory getWorldCupFactory() {
		return (WorldCupFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		worldCupEClass = createEClass(WORLD_CUP);
		createEAttribute(worldCupEClass, WORLD_CUP__YEAR);
		createEReference(worldCupEClass, WORLD_CUP__HOST);
		createEReference(worldCupEClass, WORLD_CUP__CHAMPION);
		createEReference(worldCupEClass, WORLD_CUP__ROUNDS);
		createEReference(worldCupEClass, WORLD_CUP__GROUPS);
		createEOperation(worldCupEClass, WORLD_CUP___GET_ROUND__EROUNDTYPE);
		createEOperation(worldCupEClass, WORLD_CUP___GET_GROUP__EGROUPTYPE);

		countriesEClass = createEClass(COUNTRIES);
		createEReference(countriesEClass, COUNTRIES__COUNTRIES);

		countryEClass = createEClass(COUNTRY);
		createEAttribute(countryEClass, COUNTRY__NAME);

		hostEClass = createEClass(HOST);
		createEReference(hostEClass, HOST__COUNTRY);
		createEReference(hostEClass, HOST__CITIES);

		stadiumEClass = createEClass(STADIUM);
		createEAttribute(stadiumEClass, STADIUM__NAME);

		cityEClass = createEClass(CITY);
		createEAttribute(cityEClass, CITY__NAME);
		createEReference(cityEClass, CITY__STADIUMS);

		groupEClass = createEClass(GROUP);
		createEAttribute(groupEClass, GROUP__GROUP_TYPE);
		createEReference(groupEClass, GROUP__MATCHES);
		createEReference(groupEClass, GROUP__TEAMS);
		createEOperation(groupEClass, GROUP___GET_BEST_TEAM);
		createEOperation(groupEClass, GROUP___GET_SECOND_BEST_TEAM);

		roundEClass = createEClass(ROUND);
		createEReference(roundEClass, ROUND__MATCHES);
		createEAttribute(roundEClass, ROUND__ROUND_TYPE);
		createEReference(roundEClass, ROUND__WORLD_CUP);
		createEOperation(roundEClass, ROUND___GET_NEXT_ROUND);

		matchEClass = createEClass(MATCH);
		createEReference(matchEClass, MATCH__STADIUM);
		createEReference(matchEClass, MATCH__HOME);
		createEReference(matchEClass, MATCH__AWAY);
		createEAttribute(matchEClass, MATCH__SCORE);
		createEAttribute(matchEClass, MATCH__TIME);
		createEAttribute(matchEClass, MATCH__DATE);
		createEOperation(matchEClass, MATCH___GET_WINNER);
		createEOperation(matchEClass, MATCH___GET_LOSER);
		createEOperation(matchEClass, MATCH___IS_DRAW);

		groupMatchEClass = createEClass(GROUP_MATCH);

		finalMatchEClass = createEClass(FINAL_MATCH);
		createEAttribute(finalMatchEClass, FINAL_MATCH__USED_EXTRA_TIME);
		createEAttribute(finalMatchEClass, FINAL_MATCH__USED_PENALTIES);
		createEAttribute(finalMatchEClass, FINAL_MATCH__PENALTY_SCORE);
		createEReference(finalMatchEClass, FINAL_MATCH__ROUND);
		createEOperation(finalMatchEClass, FINAL_MATCH___GET_FOLLOWING_MATCH);

		teamEClass = createEClass(TEAM);
		createEReference(teamEClass, TEAM__COACH);
		createEReference(teamEClass, TEAM__GROUP);
		createEReference(teamEClass, TEAM__GROUP_STATS);
		createEReference(teamEClass, TEAM__COUNTRY);
		createEReference(teamEClass, TEAM__PLAYERS);

		groupStatsEClass = createEClass(GROUP_STATS);
		createEReference(groupStatsEClass, GROUP_STATS__TEAM);
		createEAttribute(groupStatsEClass, GROUP_STATS__MATCHES_PLAYED);
		createEAttribute(groupStatsEClass, GROUP_STATS__WON);
		createEAttribute(groupStatsEClass, GROUP_STATS__DRAW);
		createEAttribute(groupStatsEClass, GROUP_STATS__LOST);
		createEAttribute(groupStatsEClass, GROUP_STATS__GOALS_FOR);
		createEAttribute(groupStatsEClass, GROUP_STATS__GOALS_AGAINST);
		createEAttribute(groupStatsEClass, GROUP_STATS__GOAL_DIFFERENCE);
		createEAttribute(groupStatsEClass, GROUP_STATS__POINTS);

		playerEClass = createEClass(PLAYER);
		createEAttribute(playerEClass, PLAYER__POSITION);
		createEAttribute(playerEClass, PLAYER__SHIRT_NUMBER);

		coachEClass = createEClass(COACH);
		createEReference(coachEClass, COACH__TEAM);

		personEClass = createEClass(PERSON);
		createEAttribute(personEClass, PERSON__FIRST_NAME);
		createEAttribute(personEClass, PERSON__SURNAME);
		createEAttribute(personEClass, PERSON__FULL_NAME);

		// Create enums
		eGroupTypeEEnum = createEEnum(EGROUP_TYPE);
		eRoundTypeEEnum = createEEnum(EROUND_TYPE);
		ePositionEEnum = createEEnum(EPOSITION);

		// Create data types
		eDateEDataType = createEDataType(EDATE);
		eScoreEDataType = createEDataType(ESCORE);
		eTimeEDataType = createEDataType(ETIME);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This method is
	 * guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		groupMatchEClass.getESuperTypes().add(this.getMatch());
		finalMatchEClass.getESuperTypes().add(this.getMatch());
		playerEClass.getESuperTypes().add(this.getPerson());
		coachEClass.getESuperTypes().add(this.getPerson());

		// Initialize classes, features, and operations; add parameters
		initEClass(worldCupEClass, WorldCup.class, "WorldCup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWorldCup_Year(), ecorePackage.getEInt(), "year", null, 0, 1, WorldCup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWorldCup_Host(), this.getHost(), null, "host", null, 0, 1, WorldCup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWorldCup_Champion(), this.getTeam(), null, "champion", null, 0, 1, WorldCup.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getWorldCup_Rounds(), this.getRound(), this.getRound_WorldCup(), "rounds", null, 0, 5, WorldCup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWorldCup_Groups(), this.getGroup(), null, "groups", null, 0, 8, WorldCup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getWorldCup__GetRound__ERoundType(), this.getRound(), "getRound", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getERoundType(), "roundType", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getWorldCup__GetGroup__EGroupType(), this.getGroup(), "getGroup", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEGroupType(), "groupType", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(countriesEClass, Countries.class, "Countries", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCountries_Countries(), this.getCountry(), null, "countries", null, 0, -1, Countries.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(countryEClass, Country.class, "Country", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCountry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Country.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hostEClass, Host.class, "Host", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHost_Country(), this.getCountry(), null, "country", null, 0, 1, Host.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHost_Cities(), this.getCity(), null, "cities", null, 0, -1, Host.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stadiumEClass, Stadium.class, "Stadium", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStadium_Name(), ecorePackage.getEString(), "name", null, 0, 1, Stadium.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cityEClass, City.class, "City", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCity_Name(), ecorePackage.getEString(), "name", null, 0, 1, City.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCity_Stadiums(), this.getStadium(), null, "stadiums", null, 0, -1, City.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(groupEClass, Group.class, "Group", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGroup_GroupType(), this.getEGroupType(), "groupType", null, 0, 1, Group.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGroup_Matches(), this.getGroupMatch(), null, "matches", null, 0, 6, Group.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGroup_Teams(), this.getTeam(), this.getTeam_Group(), "teams", null, 0, 4, Group.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getGroup__GetBestTeam(), this.getTeam(), "getBestTeam", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getGroup__GetSecondBestTeam(), this.getTeam(), "getSecondBestTeam", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(roundEClass, Round.class, "Round", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRound_Matches(), this.getFinalMatch(), this.getFinalMatch_Round(), "matches", null, 0, -1, Round.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRound_RoundType(), this.getERoundType(), "roundType", null, 0, 1, Round.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRound_WorldCup(), this.getWorldCup(), this.getWorldCup_Rounds(), "worldCup", null, 0, 1, Round.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getRound__GetNextRound(), this.getRound(), "getNextRound", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(matchEClass, Match.class, "Match", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMatch_Stadium(), this.getStadium(), null, "stadium", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMatch_Home(), this.getTeam(), null, "home", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMatch_Away(), this.getTeam(), null, "away", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMatch_Score(), this.getEScore(), "score", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMatch_Time(), this.getETime(), "time", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMatch_Date(), this.getEDate(), "date", null, 0, 1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMatch__GetWinner(), this.getTeam(), "getWinner", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMatch__GetLoser(), this.getTeam(), "getLoser", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMatch__IsDraw(), ecorePackage.getEBoolean(), "isDraw", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(groupMatchEClass, GroupMatch.class, "GroupMatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(finalMatchEClass, FinalMatch.class, "FinalMatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFinalMatch_UsedExtraTime(), ecorePackage.getEBoolean(), "usedExtraTime", "false", 0, 1, FinalMatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFinalMatch_UsedPenalties(), ecorePackage.getEBoolean(), "usedPenalties", "false", 0, 1, FinalMatch.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getFinalMatch_PenaltyScore(), this.getEScore(), "penaltyScore", null, 0, 1, FinalMatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFinalMatch_Round(), this.getRound(), this.getRound_Matches(), "round", null, 0, 1, FinalMatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getFinalMatch__GetFollowingMatch(), this.getFinalMatch(), "getFollowingMatch", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(teamEClass, Team.class, "Team", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTeam_Coach(), this.getCoach(), this.getCoach_Team(), "coach", null, 0, 1, Team.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTeam_Group(), this.getGroup(), this.getGroup_Teams(), "group", null, 0, 1, Team.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTeam_GroupStats(), this.getGroupStats(), this.getGroupStats_Team(), "groupStats", null, 0, 1, Team.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTeam_Country(), this.getCountry(), null, "country", null, 0, 1, Team.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTeam_Players(), this.getPlayer(), null, "players", null, 0, -1, Team.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(groupStatsEClass, GroupStats.class, "GroupStats", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroupStats_Team(), this.getTeam(), this.getTeam_GroupStats(), "team", null, 0, 1, GroupStats.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_MatchesPlayed(), ecorePackage.getEInt(), "matchesPlayed", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_Won(), ecorePackage.getEInt(), "won", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_Draw(), ecorePackage.getEInt(), "draw", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_Lost(), ecorePackage.getEInt(), "lost", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_GoalsFor(), ecorePackage.getEInt(), "goalsFor", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_GoalsAgainst(), ecorePackage.getEInt(), "goalsAgainst", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_GoalDifference(), ecorePackage.getEInt(), "goalDifference", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGroupStats_Points(), ecorePackage.getEInt(), "points", null, 0, 1, GroupStats.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(playerEClass, Player.class, "Player", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPlayer_Position(), this.getEPosition(), "position", null, 0, 1, Player.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlayer_ShirtNumber(), ecorePackage.getEInt(), "shirtNumber", "1", 0, 1, Player.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(coachEClass, Coach.class, "Coach", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCoach_Team(), this.getTeam(), this.getTeam_Coach(), "team", null, 0, 1, Coach.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personEClass, Person.class, "Person", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerson_FirstName(), ecorePackage.getEString(), "firstName", null, 0, 1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_Surname(), ecorePackage.getEString(), "surname", null, 0, 1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_FullName(), ecorePackage.getEString(), "fullName", null, 0, 1, Person.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(eGroupTypeEEnum, EGroupType.class, "EGroupType");
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_A);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_B);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_C);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_D);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_E);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_F);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_G);
		addEEnumLiteral(eGroupTypeEEnum, EGroupType.GROUP_H);

		initEEnum(eRoundTypeEEnum, ERoundType.class, "ERoundType");
		addEEnumLiteral(eRoundTypeEEnum, ERoundType.ROUND_OF_16);
		addEEnumLiteral(eRoundTypeEEnum, ERoundType.QUARTERFINALS);
		addEEnumLiteral(eRoundTypeEEnum, ERoundType.SEMIFINALS);
		addEEnumLiteral(eRoundTypeEEnum, ERoundType.THIRD_PLACE_PLAYOFF);
		addEEnumLiteral(eRoundTypeEEnum, ERoundType.FINAL);

		initEEnum(ePositionEEnum, EPosition.class, "EPosition");
		addEEnumLiteral(ePositionEEnum, EPosition.GOALKEEPER);
		addEEnumLiteral(ePositionEEnum, EPosition.DEFENDER);
		addEEnumLiteral(ePositionEEnum, EPosition.MIDFIELDER);
		addEEnumLiteral(ePositionEEnum, EPosition.FORWARD);

		// Initialize data types
		initEDataType(eDateEDataType, Date.class, "EDate", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eScoreEDataType, Score.class, "EScore", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eTimeEDataType, Time.class, "ETime", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/acceleo/query/1.0
		create_1Annotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "validationDelegates", "http://www.eclipse.org/acceleo/query/1.0"
		   });
		addAnnotation
		  (worldCupEClass,
		   source,
		   new String[] {
			   "constraints", "hostTakesPart eightGroups distinctGroupTypes containsGroupA containsGroupB containsGroupC containsGroupD containsGroupE containsGroupF containsGroupG containsGroupH eachTeamCountryUsedOnce fiveRounds distinctRoundTypes containsRoundOf16 containsQuarterFinals containsSemiFinals containsThirdPlacePlayoff containsFinal winnersRoundOf16ToQuarterFinals winnersQuarterFinalsToSemiFinals winnersSemiFinalsToFinal losersSemiFinalsToThirdPlacePlayoff best2TeamsEachGroupToRoundOf16 matchesNotAtSameTimeInSameStadium groupMatchesBeforeFinalMatches"
		   });
		addAnnotation
		  (groupEClass,
		   source,
		   new String[] {
			   "constraints", "sixMatches fourTeams distinctTeamCountries"
		   });
		addAnnotation
		  (roundEClass,
		   source,
		   new String[] {
			   "constraints", "eightMatchesRoundOf16 fourMatchesQuarterFinals twoMatchesSemiFinals oneMatchThirdPlacePlayoff oneMatchFinal"
		   });
		addAnnotation
		  (finalMatchEClass,
		   source,
		   new String[] {
			   "constraints", "penaltyScoreWithinRange mustHaveWinnerAndLoser"
		   });
		addAnnotation
		  (teamEClass,
		   source,
		   new String[] {
			   "constraints", "threeGroupMatchesPlayed noDuplicateShirtNumbers atLeastElevenPlayers"
		   });
		addAnnotation
		  (playerEClass,
		   source,
		   new String[] {
			   "constraints", "shirtNumberWithinRange"
		   });
		addAnnotation
		  (eDateEDataType,
		   source,
		   new String[] {
			   "constraints", "dateWithinRange"
		   });
		addAnnotation
		  (eScoreEDataType,
		   source,
		   new String[] {
			   "constraints", "scoreWithinRange"
		   });
		addAnnotation
		  (eTimeEDataType,
		   source,
		   new String[] {
			   "constraints", "timeWithinRange"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/acceleo/query/1.0</b>.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	protected void create_1Annotations() {
		String source = "http://www.eclipse.org/acceleo/query/1.0";
		addAnnotation
		  (worldCupEClass,
		   source,
		   new String[] {
			   "hostTakesPart", "self.groups->exists(group | group.teams->exists(team | team.country = self.host.country))",
			   "eightGroups", "self.groups->size() = 8",
			   "distinctGroupTypes", "self.groups->isUnique(group | group.groupType)",
			   "containsGroupA", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_A)",
			   "containsGroupB", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_B)",
			   "containsGroupC", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_C)",
			   "containsGroupD", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_D)",
			   "containsGroupE", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_E)",
			   "containsGroupF", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_F)",
			   "containsGroupG", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_G)",
			   "containsGroupH", "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_H)",
			   "eachTeamCountryUsedOnce", "self.groups->collect(group | group.teams)->isUnique(team | team.country)",
			   "fiveRounds", "self.rounds->size() = 5",
			   "distinctRoundTypes", "self.rounds->isUnique(round | round.roundType)",
			   "containsRoundOf16", "self.rounds->exists(round | round.roundType = worldcup::ERoundType::ROUND_OF_16)",
			   "containsQuarterFinals", "self.rounds->exists(round | round.roundType = worldcup::ERoundType::QUARTERFINALS)",
			   "containsSemiFinals", "self.rounds->exists(round | round.roundType = worldcup::ERoundType::SEMIFINALS)",
			   "containsThirdPlacePlayoff", "self.rounds->exists(round | round.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF)",
			   "containsFinal", "self.rounds->exists(round | round.roundType = worldcup::ERoundType::FINAL)"
		   });
		addAnnotation
		  (groupEClass,
		   source,
		   new String[] {
			   "sixMatches", "self.matches->size() = 6",
			   "fourTeams", "self.teams->size() = 4",
			   "distinctTeamCountries", "self.teams->isUnique(team | team.country)"
		   });
		addAnnotation
		  (roundEClass,
		   source,
		   new String[] {
			   "eightMatchesRoundOf16", "if (self.roundType = worldcup::ERoundType::ROUND_OF_16) then (self.matches->size() = 8) else true endif",
			   "fourMatchesQuarterFinals", "if (self.roundType = worldcup::ERoundType::QUARTERFINALS) then (self.matches->size() = 4) else true endif",
			   "twoMatchesSemiFinals", "if (self.roundType = worldcup::ERoundType::SEMIFINALS) then (self.matches->size() = 2) else true endif",
			   "oneMatchThirdPlacePlayoff", "if (self.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF) then (self.matches->size() = 1) else true endif",
			   "oneMatchFinal", "if (self.roundType = worldcup::ERoundType::FINAL) then (self.matches->size() = 1) else true endif"
		   });
		addAnnotation
		  (finalMatchEClass,
		   source,
		   new String[] {
			   "mustHaveWinnerAndLoser", "self.getWinner() != null and self.getLoser() != null"
		   });
		addAnnotation
		  (teamEClass,
		   source,
		   new String[] {
			   "threeGroupMatchesPlayed", "self.group.matches->select(match | match.home = self or match.away = self)->size() = 3",
			   "noDuplicateShirtNumbers", "self.players->isUnique(player | player.shirtNumber)",
			   "atLeastElevenPlayers", "self.players->size() >= 11"
		   });
		addAnnotation
		  (playerEClass,
		   source,
		   new String[] {
			   "shirtNumberWithinRange", "self.shirtNumber > 0"
		   });
	}

} // WorldCupPackageImpl
