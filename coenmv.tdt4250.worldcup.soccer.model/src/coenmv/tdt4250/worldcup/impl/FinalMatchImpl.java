/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.FinalMatch;
import coenmv.tdt4250.worldcup.Round;
import coenmv.tdt4250.worldcup.Team;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import coenmv.tdt4250.worldcup.util.Score;

import static coenmv.tdt4250.worldcup.util.Util.equalsIgnoreNull;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Final
 * Match</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl#isUsedExtraTime <em>Used Extra Time</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl#isUsedPenalties <em>Used Penalties</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl#getPenaltyScore <em>Penalty Score</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl#getRound <em>Round</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FinalMatchImpl extends MatchImpl implements FinalMatch {
	/**
	 * The default value of the '{@link #isUsedExtraTime() <em>Used Extra Time</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isUsedExtraTime()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USED_EXTRA_TIME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUsedExtraTime() <em>Used Extra Time</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isUsedExtraTime()
	 * @generated
	 * @ordered
	 */
	protected boolean usedExtraTime = USED_EXTRA_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #isUsedPenalties() <em>Used Penalties</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isUsedPenalties()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USED_PENALTIES_EDEFAULT = false;

	/**
	 * The default value of the '{@link #getPenaltyScore() <em>Penalty Score</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPenaltyScore()
	 * @generated
	 * @ordered
	 */
	protected static final Score PENALTY_SCORE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPenaltyScore() <em>Penalty Score</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPenaltyScore()
	 * @generated
	 * @ordered
	 */
	protected Score penaltyScore = PENALTY_SCORE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FinalMatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.FINAL_MATCH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUsedExtraTime() {
		return usedExtraTime;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsedExtraTime(boolean newUsedExtraTime) {
		boolean oldUsedExtraTime = usedExtraTime;
		usedExtraTime = newUsedExtraTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.FINAL_MATCH__USED_EXTRA_TIME, oldUsedExtraTime, usedExtraTime));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isUsedPenalties() {
		if (getScore() != null) {
			int homeScore = getScore().homeScore;
			int awayScore = getScore().awayScore;
			return usedExtraTime && homeScore == awayScore;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Score getPenaltyScore() {
		return penaltyScore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPenaltyScore(Score newPenaltyScore) {
		Score oldPenaltyScore = penaltyScore;
		penaltyScore = newPenaltyScore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.FINAL_MATCH__PENALTY_SCORE, oldPenaltyScore, penaltyScore));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Round getRound() {
		if (eContainerFeatureID() != WorldCupPackage.FINAL_MATCH__ROUND) return null;
		return (Round)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRound(Round newRound, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRound, WorldCupPackage.FINAL_MATCH__ROUND, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRound(Round newRound) {
		if (newRound != eInternalContainer() || (eContainerFeatureID() != WorldCupPackage.FINAL_MATCH__ROUND && newRound != null)) {
			if (EcoreUtil.isAncestor(this, newRound))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRound != null)
				msgs = ((InternalEObject)newRound).eInverseAdd(this, WorldCupPackage.ROUND__MATCHES, Round.class, msgs);
			msgs = basicSetRound(newRound, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.FINAL_MATCH__ROUND, newRound, newRound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FinalMatch getFollowingMatch() {
		// TODO
		Round nextRound = getRound().getNextRound();
		for (FinalMatch nextMatch : nextRound.getMatches()) {
			if (equalsIgnoreNull(nextMatch.getHome(), getWinner()) || equalsIgnoreNull(nextMatch.getAway(), getWinner())) {
				return nextMatch;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__ROUND:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRound((Round)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__ROUND:
				return basicSetRound(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WorldCupPackage.FINAL_MATCH__ROUND:
				return eInternalContainer().eInverseRemove(this, WorldCupPackage.ROUND__MATCHES, Round.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__USED_EXTRA_TIME:
				return isUsedExtraTime();
			case WorldCupPackage.FINAL_MATCH__USED_PENALTIES:
				return isUsedPenalties();
			case WorldCupPackage.FINAL_MATCH__PENALTY_SCORE:
				return getPenaltyScore();
			case WorldCupPackage.FINAL_MATCH__ROUND:
				return getRound();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__USED_EXTRA_TIME:
				setUsedExtraTime((Boolean)newValue);
				return;
			case WorldCupPackage.FINAL_MATCH__PENALTY_SCORE:
				setPenaltyScore((Score)newValue);
				return;
			case WorldCupPackage.FINAL_MATCH__ROUND:
				setRound((Round)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__USED_EXTRA_TIME:
				setUsedExtraTime(USED_EXTRA_TIME_EDEFAULT);
				return;
			case WorldCupPackage.FINAL_MATCH__PENALTY_SCORE:
				setPenaltyScore(PENALTY_SCORE_EDEFAULT);
				return;
			case WorldCupPackage.FINAL_MATCH__ROUND:
				setRound((Round)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.FINAL_MATCH__USED_EXTRA_TIME:
				return usedExtraTime != USED_EXTRA_TIME_EDEFAULT;
			case WorldCupPackage.FINAL_MATCH__USED_PENALTIES:
				return isUsedPenalties() != USED_PENALTIES_EDEFAULT;
			case WorldCupPackage.FINAL_MATCH__PENALTY_SCORE:
				return PENALTY_SCORE_EDEFAULT == null ? penaltyScore != null : !PENALTY_SCORE_EDEFAULT.equals(penaltyScore);
			case WorldCupPackage.FINAL_MATCH__ROUND:
				return getRound() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldCupPackage.FINAL_MATCH___GET_FOLLOWING_MATCH:
				return getFollowingMatch();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Team getWinner() {
		// TODO
		Team winner = super.getWinner();
		if (winner != null) {
			return winner;
		}
		if (isUsedPenalties() && getPenaltyScore() != null) {
			int penaltyHomeScore = getPenaltyScore().homeScore;
			int penaltyAwayScore = getPenaltyScore().awayScore;
			if (penaltyHomeScore < penaltyAwayScore) {
				return getAway();
			} else if (penaltyHomeScore > penaltyAwayScore) {
				return getHome();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Team getLoser() {
		// TODO
		Team loser = super.getLoser();
		if (loser != null) {
			return loser;
		}
		if (isUsedPenalties() && getPenaltyScore() != null) {
			int penaltyHomeScore = getPenaltyScore().homeScore;
			int penaltyAwayScore = getPenaltyScore().awayScore;
			if (penaltyHomeScore < penaltyAwayScore) {
				return getHome();
			} else if (penaltyHomeScore > penaltyAwayScore) {
				return getAway();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public boolean isDraw() {
		// TODO
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (usedExtraTime: ");
		result.append(usedExtraTime);
		result.append(", penaltyScore: ");
		result.append(penaltyScore);
		result.append(')');
		return result.toString();
	}

} // FinalMatchImpl
