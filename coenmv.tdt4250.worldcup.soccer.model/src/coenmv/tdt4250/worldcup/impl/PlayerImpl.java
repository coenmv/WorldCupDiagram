/**
 */
package coenmv.tdt4250.worldcup.impl;

import coenmv.tdt4250.worldcup.EPosition;
import coenmv.tdt4250.worldcup.Player;
import coenmv.tdt4250.worldcup.WorldCupPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.PlayerImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.impl.PlayerImpl#getShirtNumber <em>Shirt Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlayerImpl extends PersonImpl implements Player {
	/**
	 * The default value of the '{@link #getPosition() <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final EPosition POSITION_EDEFAULT = EPosition.GOALKEEPER;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected EPosition position = POSITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getShirtNumber() <em>Shirt Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShirtNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int SHIRT_NUMBER_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getShirtNumber() <em>Shirt Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShirtNumber()
	 * @generated
	 * @ordered
	 */
	protected int shirtNumber = SHIRT_NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldCupPackage.Literals.PLAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPosition getPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(EPosition newPosition) {
		EPosition oldPosition = position;
		position = newPosition == null ? POSITION_EDEFAULT : newPosition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.PLAYER__POSITION, oldPosition, position));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getShirtNumber() {
		return shirtNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShirtNumber(int newShirtNumber) {
		int oldShirtNumber = shirtNumber;
		shirtNumber = newShirtNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldCupPackage.PLAYER__SHIRT_NUMBER, oldShirtNumber, shirtNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldCupPackage.PLAYER__POSITION:
				return getPosition();
			case WorldCupPackage.PLAYER__SHIRT_NUMBER:
				return getShirtNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldCupPackage.PLAYER__POSITION:
				setPosition((EPosition)newValue);
				return;
			case WorldCupPackage.PLAYER__SHIRT_NUMBER:
				setShirtNumber((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldCupPackage.PLAYER__POSITION:
				setPosition(POSITION_EDEFAULT);
				return;
			case WorldCupPackage.PLAYER__SHIRT_NUMBER:
				setShirtNumber(SHIRT_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldCupPackage.PLAYER__POSITION:
				return position != POSITION_EDEFAULT;
			case WorldCupPackage.PLAYER__SHIRT_NUMBER:
				return shirtNumber != SHIRT_NUMBER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (position: ");
		result.append(position);
		result.append(", shirtNumber: ");
		result.append(shirtNumber);
		result.append(')');
		return result.toString();
	}

} //PlayerImpl
