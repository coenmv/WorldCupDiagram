/**
 */
package coenmv.tdt4250.worldcup;

import coenmv.tdt4250.worldcup.util.Date;
import coenmv.tdt4250.worldcup.util.Score;
import coenmv.tdt4250.worldcup.util.Time;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getStadium <em>Stadium</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getHome <em>Home</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getAway <em>Away</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getScore <em>Score</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getTime <em>Time</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Match#getDate <em>Date</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch()
 * @model abstract="true"
 * @generated
 */
public interface Match extends EObject {
	/**
	 * Returns the value of the '<em><b>Stadium</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stadium</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stadium</em>' reference.
	 * @see #setStadium(Stadium)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Stadium()
	 * @model
	 * @generated
	 */
	Stadium getStadium();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getStadium <em>Stadium</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stadium</em>' reference.
	 * @see #getStadium()
	 * @generated
	 */
	void setStadium(Stadium value);

	/**
	 * Returns the value of the '<em><b>Home</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Home</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Home</em>' reference.
	 * @see #setHome(Team)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Home()
	 * @model
	 * @generated
	 */
	Team getHome();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getHome <em>Home</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Home</em>' reference.
	 * @see #getHome()
	 * @generated
	 */
	void setHome(Team value);

	/**
	 * Returns the value of the '<em><b>Away</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Away</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Away</em>' reference.
	 * @see #setAway(Team)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Away()
	 * @model
	 * @generated
	 */
	Team getAway();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getAway <em>Away</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Away</em>' reference.
	 * @see #getAway()
	 * @generated
	 */
	void setAway(Team value);

	/**
	 * Returns the value of the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Score</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Score</em>' attribute.
	 * @see #setScore(Score)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Score()
	 * @model dataType="coenmv.tdt4250.worldcup.EScore"
	 * @generated
	 */
	Score getScore();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getScore <em>Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Score</em>' attribute.
	 * @see #getScore()
	 * @generated
	 */
	void setScore(Score value);

	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(Time)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Time()
	 * @model dataType="coenmv.tdt4250.worldcup.ETime"
	 * @generated
	 */
	Time getTime();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Time value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getMatch_Date()
	 * @model dataType="coenmv.tdt4250.worldcup.EDate"
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Match#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Team getWinner();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Team getLoser();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isDraw();

} // Match
