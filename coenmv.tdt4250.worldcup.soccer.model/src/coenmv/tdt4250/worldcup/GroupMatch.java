/**
 */
package coenmv.tdt4250.worldcup;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Match</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupMatch()
 * @model
 * @generated
 */
public interface GroupMatch extends Match {
} // GroupMatch
