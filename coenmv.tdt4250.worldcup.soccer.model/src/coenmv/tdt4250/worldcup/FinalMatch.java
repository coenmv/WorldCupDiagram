/**
 */
package coenmv.tdt4250.worldcup;

import coenmv.tdt4250.worldcup.util.Score;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.FinalMatch#isUsedExtraTime <em>Used Extra Time</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.FinalMatch#isUsedPenalties <em>Used Penalties</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.FinalMatch#getPenaltyScore <em>Penalty Score</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.FinalMatch#getRound <em>Round</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getFinalMatch()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='penaltyScoreWithinRange mustHaveWinnerAndLoser'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 mustHaveWinnerAndLoser='self.getWinner() != null and self.getLoser() != null'"
 * @generated
 */
public interface FinalMatch extends Match {
	/**
	 * Returns the value of the '<em><b>Used Extra Time</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Extra Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Extra Time</em>' attribute.
	 * @see #setUsedExtraTime(boolean)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getFinalMatch_UsedExtraTime()
	 * @model default="false"
	 * @generated
	 */
	boolean isUsedExtraTime();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.FinalMatch#isUsedExtraTime <em>Used Extra Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Used Extra Time</em>' attribute.
	 * @see #isUsedExtraTime()
	 * @generated
	 */
	void setUsedExtraTime(boolean value);

	/**
	 * Returns the value of the '<em><b>Used Penalties</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Penalties</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Penalties</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getFinalMatch_UsedPenalties()
	 * @model default="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isUsedPenalties();

	/**
	 * Returns the value of the '<em><b>Penalty Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Penalty Score</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Penalty Score</em>' attribute.
	 * @see #setPenaltyScore(Score)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getFinalMatch_PenaltyScore()
	 * @model dataType="coenmv.tdt4250.worldcup.EScore"
	 * @generated
	 */
	Score getPenaltyScore();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.FinalMatch#getPenaltyScore <em>Penalty Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Penalty Score</em>' attribute.
	 * @see #getPenaltyScore()
	 * @generated
	 */
	void setPenaltyScore(Score value);

	/**
	 * Returns the value of the '<em><b>Round</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Round#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round</em>' container reference.
	 * @see #setRound(Round)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getFinalMatch_Round()
	 * @see coenmv.tdt4250.worldcup.Round#getMatches
	 * @model opposite="matches" transient="false"
	 * @generated
	 */
	Round getRound();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.FinalMatch#getRound <em>Round</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round</em>' container reference.
	 * @see #getRound()
	 * @generated
	 */
	void setRound(Round value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	FinalMatch getFollowingMatch();

} // FinalMatch
