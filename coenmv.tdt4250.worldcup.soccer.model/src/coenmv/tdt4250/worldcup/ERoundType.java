/**
 */
package coenmv.tdt4250.worldcup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ERound Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getERoundType()
 * @model
 * @generated
 */
public enum ERoundType implements Enumerator {
	/**
	 * The '<em><b>ROUND OF 16</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROUND_OF_16_VALUE
	 * @generated
	 * @ordered
	 */
	ROUND_OF_16(0, "ROUND_OF_16", "ROUND_OF_16"),

	/**
	 * The '<em><b>QUARTERFINALS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #QUARTERFINALS_VALUE
	 * @generated
	 * @ordered
	 */
	QUARTERFINALS(1, "QUARTERFINALS", "QUARTERFINALS"),

	/**
	 * The '<em><b>SEMIFINALS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SEMIFINALS_VALUE
	 * @generated
	 * @ordered
	 */
	SEMIFINALS(2, "SEMIFINALS", "SEMIFINALS"),

	/**
	 * The '<em><b>THIRD PLACE PLAYOFF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THIRD_PLACE_PLAYOFF_VALUE
	 * @generated
	 * @ordered
	 */
	THIRD_PLACE_PLAYOFF(3, "THIRD_PLACE_PLAYOFF", "THIRD_PLACE_PLAYOFF"),

	/**
	 * The '<em><b>FINAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FINAL_VALUE
	 * @generated
	 * @ordered
	 */
	FINAL(4, "FINAL", "FINAL");

	/**
	 * The '<em><b>ROUND OF 16</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ROUND OF 16</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROUND_OF_16
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROUND_OF_16_VALUE = 0;

	/**
	 * The '<em><b>QUARTERFINALS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>QUARTERFINALS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUARTERFINALS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int QUARTERFINALS_VALUE = 1;

	/**
	 * The '<em><b>SEMIFINALS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SEMIFINALS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SEMIFINALS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SEMIFINALS_VALUE = 2;

	/**
	 * The '<em><b>THIRD PLACE PLAYOFF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>THIRD PLACE PLAYOFF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THIRD_PLACE_PLAYOFF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int THIRD_PLACE_PLAYOFF_VALUE = 3;

	/**
	 * The '<em><b>FINAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FINAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FINAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FINAL_VALUE = 4;

	/**
	 * An array of all the '<em><b>ERound Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ERoundType[] VALUES_ARRAY =
		new ERoundType[] {
			ROUND_OF_16,
			QUARTERFINALS,
			SEMIFINALS,
			THIRD_PLACE_PLAYOFF,
			FINAL,
		};

	/**
	 * A public read-only list of all the '<em><b>ERound Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ERoundType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ERound Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ERoundType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ERoundType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ERound Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ERoundType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ERoundType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ERound Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ERoundType get(int value) {
		switch (value) {
			case ROUND_OF_16_VALUE: return ROUND_OF_16;
			case QUARTERFINALS_VALUE: return QUARTERFINALS;
			case SEMIFINALS_VALUE: return SEMIFINALS;
			case THIRD_PLACE_PLAYOFF_VALUE: return THIRD_PLACE_PLAYOFF;
			case FINAL_VALUE: return FINAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ERoundType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ERoundType
