package coenmv.tdt4250.worldcup.util;

public class Date {
	
	public int day;
	public int month;
	
	public Date(int day, int month) {
		super();
		this.day = day;
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + month;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Date))
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String dayString = String.valueOf(day);
		if (day < 10) {
			dayString = "0" + dayString;
		}
		String monthString = String.valueOf(month);
		if (month < 10) {
			monthString = "0" + monthString;
		}
		return dayString + "/" + monthString;
	}
	
	public static Date fromString(String dateString) {
		String[] dateArray = dateString.split("/");
		int day = Integer.parseInt(dateArray[0]);
		int month = Integer.parseInt(dateArray[1]);
		return new Date(day, month);
	}

}
