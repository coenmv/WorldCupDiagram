package coenmv.tdt4250.worldcup.util;



public class Score {
	
	public int homeScore;
	public int awayScore;
		
	public Score(int homeScore, int awayScore) {
		super();
		this.homeScore = homeScore;
		this.awayScore = awayScore;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getAwayScore() {
		return awayScore;
	}

	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + awayScore;
		result = prime * result + homeScore;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Score))
			return false;
		Score other = (Score) obj;
		if (awayScore != other.awayScore)
			return false;
		if (homeScore != other.homeScore)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return homeScore + "-" + awayScore;
	}
	
	public static Score fromString(String scoreString) {
		String[] scoreArray = scoreString.split("-");
		int homeScore = Integer.parseInt(scoreArray[0]);
		int awayScore = Integer.parseInt(scoreArray[1]);
		return new Score(homeScore, awayScore);
	}

}
