package coenmv.tdt4250.worldcup.util;

import java.util.Comparator;

import coenmv.tdt4250.worldcup.Team;

public class TeamComparator implements Comparator<Team> {

	@Override
	public int compare(Team team1, Team team2) {
		if (team1 == null || team2 == null) {
			return 0;
		}
		if (team1.getGroupStats() == null) {
			return -1;
		} else if (team2.getGroupStats() == null) {
			return 1;
		}
		int pointsTeam1 = team1.getGroupStats().getPoints();
		int pointsTeam2 = team2.getGroupStats().getPoints();
		if (pointsTeam1 > pointsTeam2) {
			return 1;
		} else if (pointsTeam1 < pointsTeam2) {
			return -1;
		} else {
			int goalDiffTeam1 = team1.getGroupStats().getGoalDifference();
			int goalDiffTeam2 = team2.getGroupStats().getGoalDifference();
			if (goalDiffTeam1 > goalDiffTeam2) {
				return 1;
			} else if (goalDiffTeam1 < goalDiffTeam2) {
				return -1;
			} else {
				int goalsForTeam1 = team1.getGroupStats().getGoalsFor();
				int goalsForTeam2 = team2.getGroupStats().getGoalsFor();
				if (goalsForTeam1 > goalsForTeam2) {
					return 1;
				} else if (goalsForTeam1 < goalsForTeam2) {
					return -1;
				}
			}
		}
		return 0;
	}

}
