package coenmv.tdt4250.worldcup.util;

public class Util {
	
	public static boolean equalsIgnoreNull(Object o1, Object o2) {
		if (o1 == null || o2 == null) {
			return false;
		}
		return o1.equals(o2);
	}

}
