/**
 */
package coenmv.tdt4250.worldcup.util;

import coenmv.tdt4250.worldcup.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc --> The <b>Validator</b> for the model. <!-- end-user-doc
 * -->
 * 
 * @see coenmv.tdt4250.worldcup.WorldCupPackage
 * @generated
 */
public class WorldCupValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final WorldCupValidator INSTANCE = new WorldCupValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "coenmv.tdt4250.worldcup";

	/**
	 * A constant with a fixed name that can be used as the base value for
	 * additional hand written constants. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	public WorldCupValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return WorldCupPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
			case WorldCupPackage.WORLD_CUP:
				return validateWorldCup((WorldCup)value, diagnostics, context);
			case WorldCupPackage.COUNTRIES:
				return validateCountries((Countries)value, diagnostics, context);
			case WorldCupPackage.COUNTRY:
				return validateCountry((Country)value, diagnostics, context);
			case WorldCupPackage.HOST:
				return validateHost((Host)value, diagnostics, context);
			case WorldCupPackage.STADIUM:
				return validateStadium((Stadium)value, diagnostics, context);
			case WorldCupPackage.CITY:
				return validateCity((City)value, diagnostics, context);
			case WorldCupPackage.GROUP:
				return validateGroup((Group)value, diagnostics, context);
			case WorldCupPackage.ROUND:
				return validateRound((Round)value, diagnostics, context);
			case WorldCupPackage.MATCH:
				return validateMatch((Match)value, diagnostics, context);
			case WorldCupPackage.GROUP_MATCH:
				return validateGroupMatch((GroupMatch)value, diagnostics, context);
			case WorldCupPackage.FINAL_MATCH:
				return validateFinalMatch((FinalMatch)value, diagnostics, context);
			case WorldCupPackage.TEAM:
				return validateTeam((Team)value, diagnostics, context);
			case WorldCupPackage.GROUP_STATS:
				return validateGroupStats((GroupStats)value, diagnostics, context);
			case WorldCupPackage.PLAYER:
				return validatePlayer((Player)value, diagnostics, context);
			case WorldCupPackage.COACH:
				return validateCoach((Coach)value, diagnostics, context);
			case WorldCupPackage.PERSON:
				return validatePerson((Person)value, diagnostics, context);
			case WorldCupPackage.EGROUP_TYPE:
				return validateEGroupType((EGroupType)value, diagnostics, context);
			case WorldCupPackage.EROUND_TYPE:
				return validateERoundType((ERoundType)value, diagnostics, context);
			case WorldCupPackage.EPOSITION:
				return validateEPosition((EPosition)value, diagnostics, context);
			case WorldCupPackage.EDATE:
				return validateEDate((Date)value, diagnostics, context);
			case WorldCupPackage.ESCORE:
				return validateEScore((Score)value, diagnostics, context);
			case WorldCupPackage.ETIME:
				return validateETime((Time)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorldCup(WorldCup worldCup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(worldCup, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_hostTakesPart(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_eightGroups(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_distinctGroupTypes(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupA(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupB(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupC(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupD(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupE(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupF(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupG(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsGroupH(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_eachTeamCountryUsedOnce(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_fiveRounds(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_distinctRoundTypes(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsRoundOf16(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsQuarterFinals(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsSemiFinals(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsThirdPlacePlayoff(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_containsFinal(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_winnersRoundOf16ToQuarterFinals(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_winnersQuarterFinalsToSemiFinals(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_winnersSemiFinalsToFinal(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_losersSemiFinalsToThirdPlacePlayoff(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_best2TeamsEachGroupToRoundOf16(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_matchesNotAtSameTimeInSameStadium(worldCup, diagnostics, context);
		if (result || diagnostics != null) result &= validateWorldCup_groupMatchesBeforeFinalMatches(worldCup, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the hostTakesPart constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__HOST_TAKES_PART__EEXPRESSION = "self.groups->exists(group | group.teams->exists(team | team.country = self.host.country))";

	/**
	 * Validates the hostTakesPart constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_hostTakesPart(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "hostTakesPart",
				 WORLD_CUP__HOST_TAKES_PART__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the eightGroups constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__EIGHT_GROUPS__EEXPRESSION = "self.groups->size() = 8";

	/**
	 * Validates the eightGroups constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_eightGroups(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "eightGroups",
				 WORLD_CUP__EIGHT_GROUPS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the distinctGroupTypes constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__DISTINCT_GROUP_TYPES__EEXPRESSION = "self.groups->isUnique(group | group.groupType)";

	/**
	 * Validates the distinctGroupTypes constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_distinctGroupTypes(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "distinctGroupTypes",
				 WORLD_CUP__DISTINCT_GROUP_TYPES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupA constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_A__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_A)";

	/**
	 * Validates the containsGroupA constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupA(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupA",
				 WORLD_CUP__CONTAINS_GROUP_A__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupB constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_B__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_B)";

	/**
	 * Validates the containsGroupB constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupB(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupB",
				 WORLD_CUP__CONTAINS_GROUP_B__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupC constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_C__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_C)";

	/**
	 * Validates the containsGroupC constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupC(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupC",
				 WORLD_CUP__CONTAINS_GROUP_C__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupD constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_D__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_D)";

	/**
	 * Validates the containsGroupD constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupD(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupD",
				 WORLD_CUP__CONTAINS_GROUP_D__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupE constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_E__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_E)";

	/**
	 * Validates the containsGroupE constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupE(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupE",
				 WORLD_CUP__CONTAINS_GROUP_E__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupF constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_F__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_F)";

	/**
	 * Validates the containsGroupF constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupF(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupF",
				 WORLD_CUP__CONTAINS_GROUP_F__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupG constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_G__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_G)";

	/**
	 * Validates the containsGroupG constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupG(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupG",
				 WORLD_CUP__CONTAINS_GROUP_G__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsGroupH constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_GROUP_H__EEXPRESSION = "self.groups->exists(group | group.groupType = worldcup::EGroupType::GROUP_H)";

	/**
	 * Validates the containsGroupH constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsGroupH(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsGroupH",
				 WORLD_CUP__CONTAINS_GROUP_H__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the eachTeamCountryUsedOnce constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__EACH_TEAM_COUNTRY_USED_ONCE__EEXPRESSION = "self.groups->collect(group | group.teams)->isUnique(team | team.country)";

	/**
	 * Validates the eachTeamCountryUsedOnce constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorldCup_eachTeamCountryUsedOnce(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "eachTeamCountryUsedOnce",
				 WORLD_CUP__EACH_TEAM_COUNTRY_USED_ONCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the fiveRounds constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__FIVE_ROUNDS__EEXPRESSION = "self.rounds->size() = 5";

	/**
	 * Validates the fiveRounds constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_fiveRounds(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "fiveRounds",
				 WORLD_CUP__FIVE_ROUNDS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the distinctRoundTypes constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__DISTINCT_ROUND_TYPES__EEXPRESSION = "self.rounds->isUnique(round | round.roundType)";

	/**
	 * Validates the distinctRoundTypes constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_distinctRoundTypes(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "distinctRoundTypes",
				 WORLD_CUP__DISTINCT_ROUND_TYPES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsRoundOf16 constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_ROUND_OF16__EEXPRESSION = "self.rounds->exists(round | round.roundType = worldcup::ERoundType::ROUND_OF_16)";

	/**
	 * Validates the containsRoundOf16 constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsRoundOf16(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsRoundOf16",
				 WORLD_CUP__CONTAINS_ROUND_OF16__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsQuarterFinals constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_QUARTER_FINALS__EEXPRESSION = "self.rounds->exists(round | round.roundType = worldcup::ERoundType::QUARTERFINALS)";

	/**
	 * Validates the containsQuarterFinals constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsQuarterFinals(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsQuarterFinals",
				 WORLD_CUP__CONTAINS_QUARTER_FINALS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsSemiFinals constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_SEMI_FINALS__EEXPRESSION = "self.rounds->exists(round | round.roundType = worldcup::ERoundType::SEMIFINALS)";

	/**
	 * Validates the containsSemiFinals constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsSemiFinals(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsSemiFinals",
				 WORLD_CUP__CONTAINS_SEMI_FINALS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsThirdPlacePlayoff constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_THIRD_PLACE_PLAYOFF__EEXPRESSION = "self.rounds->exists(round | round.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF)";

	/**
	 * Validates the containsThirdPlacePlayoff constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorldCup_containsThirdPlacePlayoff(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsThirdPlacePlayoff",
				 WORLD_CUP__CONTAINS_THIRD_PLACE_PLAYOFF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the containsFinal constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WORLD_CUP__CONTAINS_FINAL__EEXPRESSION = "self.rounds->exists(round | round.roundType = worldcup::ERoundType::FINAL)";

	/**
	 * Validates the containsFinal constraint of '<em>World Cup</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateWorldCup_containsFinal(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.WORLD_CUP,
				 worldCup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "containsFinal",
				 WORLD_CUP__CONTAINS_FINAL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Validates the winnersRoundOf16ToQuarterFinals constraint of '<em>World
	 * Cup</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateWorldCup_winnersRoundOf16ToQuarterFinals(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		EList<FinalMatch> roundOf16Matches = worldCup.getRound(ERoundType.ROUND_OF_16).getMatches();
		List<Team> roundOf16Winners = roundOf16Matches.stream().map(match -> match.getWinner())
				.collect(Collectors.toList());
		for (FinalMatch match : worldCup.getRound(ERoundType.QUARTERFINALS).getMatches()) {
			if (!roundOf16Winners.contains(match.getHome()) || !roundOf16Winners.contains(match.getAway())) {
				if (diagnostics != null) {
					diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
							"_UI_GenericConstraint_diagnostic",
							new Object[] { "winnersRoundOf16ToQuarterFinals", getObjectLabel(worldCup, context) },
							new Object[] { worldCup }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates the winnersQuarterFinalsToSemiFinals constraint of '<em>World
	 * Cup</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateWorldCup_winnersQuarterFinalsToSemiFinals(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		EList<FinalMatch> quarterFinalsMatches = worldCup.getRound(ERoundType.QUARTERFINALS).getMatches();
		List<Team> quarterFinalsWinners = quarterFinalsMatches.stream().map(match -> match.getWinner())
				.collect(Collectors.toList());
		for (FinalMatch match : worldCup.getRound(ERoundType.SEMIFINALS).getMatches()) {
			if (!quarterFinalsWinners.contains(match.getHome()) || !quarterFinalsWinners.contains(match.getAway())) {
				if (diagnostics != null) {
					diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
							"_UI_GenericConstraint_diagnostic",
							new Object[] { "winnersQuarterFinalsToSemiFinals", getObjectLabel(worldCup, context) },
							new Object[] { worldCup }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates the winnersSemiFinalsToFinal constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateWorldCup_winnersSemiFinalsToFinal(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		EList<FinalMatch> semiFinalsMatches = worldCup.getRound(ERoundType.SEMIFINALS).getMatches();
		List<Team> semiFinalsWinners = semiFinalsMatches.stream().map(match -> match.getWinner())
				.collect(Collectors.toList());
		for (FinalMatch match : worldCup.getRound(ERoundType.FINAL).getMatches()) {
			if (!semiFinalsWinners.contains(match.getHome()) || !semiFinalsWinners.contains(match.getAway())) {
				if (diagnostics != null) {
					diagnostics.add(
							createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
									new Object[] { "winnersSemiFinalsToFinal", getObjectLabel(worldCup, context) },
									new Object[] { worldCup }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates the losersSemiFinalsToThirdPlacePlayoff constraint of '<em>World
	 * Cup</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateWorldCup_losersSemiFinalsToThirdPlacePlayoff(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		EList<FinalMatch> semiFinalsMatches = worldCup.getRound(ERoundType.SEMIFINALS).getMatches();
		List<Team> semiFinalsLosers = semiFinalsMatches.stream().map(match -> match.getLoser())
				.collect(Collectors.toList());
		for (FinalMatch match : worldCup.getRound(ERoundType.THIRD_PLACE_PLAYOFF).getMatches()) {
			if (!semiFinalsLosers.contains(match.getHome()) || !semiFinalsLosers.contains(match.getAway())) {
				if (diagnostics != null) {
					diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
							"_UI_GenericConstraint_diagnostic",
							new Object[] { "losersSemiFinalsToThirdPlacePlayoff", getObjectLabel(worldCup, context) },
							new Object[] { worldCup }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates the best2TeamsEachGroupToRoundOf16 constraint of '<em>World
	 * Cup</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateWorldCup_best2TeamsEachGroupToRoundOf16(WorldCup worldCup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		EList<FinalMatch> roundOf16Matches = worldCup.getRound(ERoundType.ROUND_OF_16).getMatches();
		EList<Group> groups = worldCup.getGroups();
		for (Group group : groups) {
			Team bestTeam = group.getBestTeam();
			Team secondBestTeam = group.getSecondBestTeam();
			boolean valid1 = false, valid2 = false;
			for (FinalMatch match : roundOf16Matches) {
				if (match.getHome().equals(bestTeam) || match.getAway().equals(bestTeam)) {
					valid1 = true;
				} else if (match.getHome().equals(secondBestTeam) || match.getAway().equals(secondBestTeam)) {
					valid2 = true;
				}
				if (valid1 && valid2) {
					break;
				}
			}
			boolean valid = valid1 && valid2;
			if (!valid) {
				if (diagnostics != null) {
					diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
							"_UI_GenericConstraint_diagnostic",
							new Object[] { "best2TeamsEachGroupToRoundOf16", getObjectLabel(worldCup, context) },
							new Object[] { worldCup }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates the matchesNotAtSameTimeInSameStadium constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateWorldCup_matchesNotAtSameTimeInSameStadium(WorldCup worldCup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO
		List<Match> matches = new ArrayList<Match>();
		for (Group group : worldCup.getGroups()) {
			matches.addAll(group.getMatches());
		}
		for (Round round : worldCup.getRounds()) {
			matches.addAll(round.getMatches());
		}
		for (int i = 0; i < matches.size(); i++) {
			Match m1 = matches.get(i);
			for (int j = i + 1; j < matches.size(); j++) {
				Match m2 = matches.get(j);
				if (m1.getStadium().equals(m2.getStadium())) {
					if (m1.getDate().equals(m2.getDate()) && m1.getTime().equals(m2.getTime())) {
						if (diagnostics != null) {
							diagnostics.add
								(createDiagnostic
									(Diagnostic.ERROR,
									 DIAGNOSTIC_SOURCE,
									 0,
									 "_UI_GenericConstraint_diagnostic",
									 new Object[] { "matchesNotAtSameTimeInSameStadium", getObjectLabel(worldCup, context) },
									 new Object[] { worldCup },
									 context));
						}
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Validates the groupMatchesBeforeFinalMatches constraint of '<em>World Cup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateWorldCup_groupMatchesBeforeFinalMatches(WorldCup worldCup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO
		List<GroupMatch> groupMatches = new ArrayList<GroupMatch>();
		for (Group group : worldCup.getGroups()) {
			groupMatches.addAll(group.getMatches());
		}
		List<FinalMatch> finalMatches = new ArrayList<FinalMatch>();
		for (Round round : worldCup.getRounds()) {
			finalMatches.addAll(round.getMatches());
		}
		DateComparator dateComparator = new DateComparator();
		TimeComparator timeComparator = new TimeComparator();
		for (GroupMatch m1 : groupMatches) {
			for (FinalMatch m2 : finalMatches) {
				int dateComp = dateComparator.compare(m1.getDate(), m2.getDate());
				boolean invalid = dateComp > 0;
				if (dateComp == 0) {
					int timeComp = timeComparator.compare(m1.getTime(), m2.getTime());
					invalid = invalid || (timeComp >= 0);
				}
				if (invalid) {
					if (diagnostics != null) {
						diagnostics.add
							(createDiagnostic
								(Diagnostic.ERROR,
								 DIAGNOSTIC_SOURCE,
								 0,
								 "_UI_GenericConstraint_diagnostic",
								 new Object[] { "groupMatchesBeforeFinalMatches", getObjectLabel(worldCup, context) },
								 new Object[] { worldCup },
								 context));
					}
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCountries(Countries countries, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(countries, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCountry(Country country, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(country, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHost(Host host, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(host, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStadium(Stadium stadium, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stadium, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCity(City city, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(city, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroup(Group group, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(group, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(group, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(group, diagnostics, context);
		if (result || diagnostics != null) result &= validateGroup_sixMatches(group, diagnostics, context);
		if (result || diagnostics != null) result &= validateGroup_fourTeams(group, diagnostics, context);
		if (result || diagnostics != null) result &= validateGroup_distinctTeamCountries(group, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the sixMatches constraint of '<em>Group</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String GROUP__SIX_MATCHES__EEXPRESSION = "self.matches->size() = 6";

	/**
	 * Validates the sixMatches constraint of '<em>Group</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroup_sixMatches(Group group, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.GROUP,
				 group,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "sixMatches",
				 GROUP__SIX_MATCHES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the fourTeams constraint of '<em>Group</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String GROUP__FOUR_TEAMS__EEXPRESSION = "self.teams->size() = 4";

	/**
	 * Validates the fourTeams constraint of '<em>Group</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroup_fourTeams(Group group, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.GROUP,
				 group,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "fourTeams",
				 GROUP__FOUR_TEAMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the distinctTeamCountries constraint of '<em>Group</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String GROUP__DISTINCT_TEAM_COUNTRIES__EEXPRESSION = "self.teams->isUnique(team | team.country)";

	/**
	 * Validates the distinctTeamCountries constraint of '<em>Group</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateGroup_distinctTeamCountries(Group group, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.GROUP,
				 group,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "distinctTeamCountries",
				 GROUP__DISTINCT_TEAM_COUNTRIES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRound(Round round, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(round, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(round, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(round, diagnostics, context);
		if (result || diagnostics != null) result &= validateRound_eightMatchesRoundOf16(round, diagnostics, context);
		if (result || diagnostics != null) result &= validateRound_fourMatchesQuarterFinals(round, diagnostics, context);
		if (result || diagnostics != null) result &= validateRound_twoMatchesSemiFinals(round, diagnostics, context);
		if (result || diagnostics != null) result &= validateRound_oneMatchThirdPlacePlayoff(round, diagnostics, context);
		if (result || diagnostics != null) result &= validateRound_oneMatchFinal(round, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the eightMatchesRoundOf16 constraint of '<em>Round</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROUND__EIGHT_MATCHES_ROUND_OF16__EEXPRESSION = "if (self.roundType = worldcup::ERoundType::ROUND_OF_16) then (self.matches->size() = 8) else true endif";

	/**
	 * Validates the eightMatchesRoundOf16 constraint of '<em>Round</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateRound_eightMatchesRoundOf16(Round round, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.ROUND,
				 round,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "eightMatchesRoundOf16",
				 ROUND__EIGHT_MATCHES_ROUND_OF16__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the fourMatchesQuarterFinals constraint of '<em>Round</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROUND__FOUR_MATCHES_QUARTER_FINALS__EEXPRESSION = "if (self.roundType = worldcup::ERoundType::QUARTERFINALS) then (self.matches->size() = 4) else true endif";

	/**
	 * Validates the fourMatchesQuarterFinals constraint of '<em>Round</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateRound_fourMatchesQuarterFinals(Round round, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.ROUND,
				 round,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "fourMatchesQuarterFinals",
				 ROUND__FOUR_MATCHES_QUARTER_FINALS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the twoMatchesSemiFinals constraint of '<em>Round</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROUND__TWO_MATCHES_SEMI_FINALS__EEXPRESSION = "if (self.roundType = worldcup::ERoundType::SEMIFINALS) then (self.matches->size() = 2) else true endif";

	/**
	 * Validates the twoMatchesSemiFinals constraint of '<em>Round</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateRound_twoMatchesSemiFinals(Round round, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.ROUND,
				 round,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "twoMatchesSemiFinals",
				 ROUND__TWO_MATCHES_SEMI_FINALS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the oneMatchThirdPlacePlayoff constraint of '<em>Round</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROUND__ONE_MATCH_THIRD_PLACE_PLAYOFF__EEXPRESSION = "if (self.roundType = worldcup::ERoundType::THIRD_PLACE_PLAYOFF) then (self.matches->size() = 1) else true endif";

	/**
	 * Validates the oneMatchThirdPlacePlayoff constraint of '<em>Round</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateRound_oneMatchThirdPlacePlayoff(Round round, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.ROUND,
				 round,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "oneMatchThirdPlacePlayoff",
				 ROUND__ONE_MATCH_THIRD_PLACE_PLAYOFF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the oneMatchFinal constraint of '<em>Round</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROUND__ONE_MATCH_FINAL__EEXPRESSION = "if (self.roundType = worldcup::ERoundType::FINAL) then (self.matches->size() = 1) else true endif";

	/**
	 * Validates the oneMatchFinal constraint of '<em>Round</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateRound_oneMatchFinal(Round round, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.ROUND,
				 round,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "oneMatchFinal",
				 ROUND__ONE_MATCH_FINAL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMatch(Match match, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(match, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroupMatch(GroupMatch groupMatch, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(groupMatch, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFinalMatch(FinalMatch finalMatch, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(finalMatch, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validateFinalMatch_penaltyScoreWithinRange(finalMatch, diagnostics, context);
		if (result || diagnostics != null) result &= validateFinalMatch_mustHaveWinnerAndLoser(finalMatch, diagnostics, context);
		return result;
	}

	/**
	 * Validates the penaltyScoreWithinRange constraint of '<em>Final Match</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateFinalMatch_penaltyScoreWithinRange(FinalMatch finalMatch, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		if (!finalMatch.isUsedPenalties()) {
			return true;
		}
		int penaltyHomeScore = finalMatch.getPenaltyScore().homeScore;
		int penaltyAwayScore = finalMatch.getPenaltyScore().awayScore;
		int highestScore = Math.max(penaltyHomeScore, penaltyAwayScore);
		int scoreDiff = Math.abs(penaltyHomeScore - penaltyAwayScore);
		if (penaltyHomeScore < 0 || penaltyAwayScore < 0 || (highestScore > 5 && scoreDiff > 1)) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "penaltyScoreWithinRange", getObjectLabel(finalMatch, context) },
								new Object[] { finalMatch }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the mustHaveWinnerAndLoser constraint of '<em>Final Match</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String FINAL_MATCH__MUST_HAVE_WINNER_AND_LOSER__EEXPRESSION = "self.getWinner() != null and self.getLoser() != null";

	/**
	 * Validates the mustHaveWinnerAndLoser constraint of '<em>Final Match</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFinalMatch_mustHaveWinnerAndLoser(FinalMatch finalMatch, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.FINAL_MATCH,
				 finalMatch,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "mustHaveWinnerAndLoser",
				 FINAL_MATCH__MUST_HAVE_WINNER_AND_LOSER__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTeam(Team team, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(team, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(team, diagnostics, context);
		if (result || diagnostics != null) result &= validateTeam_threeGroupMatchesPlayed(team, diagnostics, context);
		if (result || diagnostics != null) result &= validateTeam_noDuplicateShirtNumbers(team, diagnostics, context);
		if (result || diagnostics != null) result &= validateTeam_atLeastElevenPlayers(team, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the threeGroupMatchesPlayed constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TEAM__THREE_GROUP_MATCHES_PLAYED__EEXPRESSION = "self.group.matches->select(match | match.home = self or match.away = self)->size() = 3";

	/**
	 * Validates the threeGroupMatchesPlayed constraint of '<em>Team</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateTeam_threeGroupMatchesPlayed(Team team, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.TEAM,
				 team,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "threeGroupMatchesPlayed",
				 TEAM__THREE_GROUP_MATCHES_PLAYED__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the noDuplicateShirtNumbers constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TEAM__NO_DUPLICATE_SHIRT_NUMBERS__EEXPRESSION = "self.players->isUnique(player | player.shirtNumber)";

	/**
	 * Validates the noDuplicateShirtNumbers constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTeam_noDuplicateShirtNumbers(Team team, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.TEAM,
				 team,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "noDuplicateShirtNumbers",
				 TEAM__NO_DUPLICATE_SHIRT_NUMBERS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the atLeastElevenPlayers constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TEAM__AT_LEAST_ELEVEN_PLAYERS__EEXPRESSION = "self.players->size() >= 11";

	/**
	 * Validates the atLeastElevenPlayers constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTeam_atLeastElevenPlayers(Team team, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.TEAM,
				 team,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "atLeastElevenPlayers",
				 TEAM__AT_LEAST_ELEVEN_PLAYERS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroupStats(GroupStats groupStats, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(groupStats, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlayer(Player player, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(player, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(player, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(player, diagnostics, context);
		if (result || diagnostics != null) result &= validatePlayer_shirtNumberWithinRange(player, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the shirtNumberWithinRange constraint of '<em>Player</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PLAYER__SHIRT_NUMBER_WITHIN_RANGE__EEXPRESSION = "self.shirtNumber > 0";

	/**
	 * Validates the shirtNumberWithinRange constraint of '<em>Player</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validatePlayer_shirtNumberWithinRange(Player player, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return
			validate
				(WorldCupPackage.Literals.PLAYER,
				 player,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "shirtNumberWithinRange",
				 PLAYER__SHIRT_NUMBER_WITHIN_RANGE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCoach(Coach coach, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(coach, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(person, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEGroupType(EGroupType eGroupType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateERoundType(ERoundType eRoundType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEPosition(EPosition ePosition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDate(Date eDate, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateEDate_dateWithinRange(eDate, diagnostics, context);
		return result;
	}

	/**
	 * Validates the dateWithinRange constraint of '<em>EDate</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateEDate_dateWithinRange(Date eDate, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO
		DateFormat format = new SimpleDateFormat("dd/MM");
		format.setLenient(false);
		String date = eDate.toString();
		try {
			format.parse(date);
		} catch (ParseException e) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "dateWithinRange",
										getValueLabel(WorldCupPackage.Literals.EDATE, eDate, context) },
								new Object[] { eDate }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEScore(Score eScore, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateEScore_scoreWithinRange(eScore, diagnostics, context);
		return result;
	}

	/**
	 * Validates the scoreWithinRange constraint of '<em>EScore</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateEScore_scoreWithinRange(Score eScore, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO
		int homeScore = eScore.getHomeScore();
		int awayScore = eScore.getAwayScore();
		if (homeScore < 0 || awayScore < 0) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "scoreWithinRange",
										getValueLabel(WorldCupPackage.Literals.ESCORE, eScore, context) },
								new Object[] { eScore }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateETime(Time eTime, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateETime_timeWithinRange(eTime, diagnostics, context);
		return result;
	}

	/**
	 * Validates the timeWithinRange constraint of '<em>ETime</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean validateETime_timeWithinRange(Time eTime, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO
		int hours = eTime.getHours();
		int minutes = eTime.getMinutes();
		if (hours < 0 || hours >= 24 || minutes < 0 || minutes >= 60) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "timeWithinRange",
										getValueLabel(WorldCupPackage.Literals.ETIME, eTime, context) },
								new Object[] { eTime }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} // WorldCupValidator
