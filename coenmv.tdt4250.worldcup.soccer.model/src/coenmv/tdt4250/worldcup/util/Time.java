package coenmv.tdt4250.worldcup.util;



public class Time {
	
	public int hours, minutes;

	public Time(int hours, int minutes) {
		super();
		this.hours = hours;
		this.minutes = minutes;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hours;
		result = prime * result + minutes;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Time))
			return false;
		Time other = (Time) obj;
		if (hours != other.hours)
			return false;
		if (minutes != other.minutes)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String hoursString = String.valueOf(hours);
		if (hours < 10) {
			hoursString = "0" + hoursString;
		}
		String minutesString = String.valueOf(minutes);
		if (minutes < 10) {
			minutesString = "0" + minutesString;
		}
		return hoursString + ":" + minutesString;
	}
	
	public static Time fromString(String timeString) {
		String[] timeArray = timeString.split(":");
		int hours = Integer.parseInt(timeArray[0]);
		int minutes = Integer.parseInt(timeArray[1]);
		return new Time(hours, minutes);
	}

}
