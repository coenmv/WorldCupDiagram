/**
 */
package coenmv.tdt4250.worldcup.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see coenmv.tdt4250.worldcup.util.WorldCupResourceFactoryImpl
 * @generated
 */
public class WorldCupResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public WorldCupResourceImpl(URI uri) {
		super(uri);
	}

} //WorldCupResourceImpl
