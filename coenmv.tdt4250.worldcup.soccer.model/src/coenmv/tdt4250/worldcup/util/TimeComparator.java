package coenmv.tdt4250.worldcup.util;

import java.util.Comparator;

public class TimeComparator implements Comparator<Time> {

	@Override
	public int compare(Time time1, Time time2) {
		if (time1 == null || time2 == null) {
			return 0;
		}
		int hours1 = time1.getHours();
		int hours2 = time2.getHours();
		if (hours1 > hours2) {
			return 1;
		} else if (hours1 < hours2) {
			return -1;
		} else {
			int mins1 = time1.getMinutes();
			int mins2 = time2.getMinutes();
			if (mins1 > mins2) {
				return 1;
			} else if (mins1 < mins2) {
				return -1;
			}
		}
		return 0;
	}
	
}
