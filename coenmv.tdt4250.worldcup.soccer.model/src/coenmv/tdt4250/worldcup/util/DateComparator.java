package coenmv.tdt4250.worldcup.util;

import java.util.Comparator;

public class DateComparator implements Comparator<Date> {

	@Override
	public int compare(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			return 0;
		}
		int month1 = date1.getMonth();
		int month2 = date2.getMonth();
		if (month1 > month2) {
			return 1;
		} else if (month1 < month2) {
			return -1;
		} else {
			int day1 = date1.getDay();
			int day2 = date2.getDay();
			if (day1 > day2) {
				return 1;
			} else if (day1 < day2) {
				return -1;
			}
		}
		return 0;
	}

}
