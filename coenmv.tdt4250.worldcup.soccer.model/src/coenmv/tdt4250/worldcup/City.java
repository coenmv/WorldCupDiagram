/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>City</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.City#getName <em>Name</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.City#getStadiums <em>Stadiums</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getCity()
 * @model
 * @generated
 */
public interface City extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getCity_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.City#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Stadiums</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Stadium}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stadiums</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stadiums</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getCity_Stadiums()
	 * @model containment="true"
	 * @generated
	 */
	EList<Stadium> getStadiums();

} // City
