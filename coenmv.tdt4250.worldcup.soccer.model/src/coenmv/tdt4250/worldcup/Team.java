/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Team</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Team#getCoach <em>Coach</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Team#getGroup <em>Group</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Team#getGroupStats <em>Group Stats</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Team#getCountry <em>Country</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Team#getPlayers <em>Players</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='threeGroupMatchesPlayed noDuplicateShirtNumbers atLeastElevenPlayers'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 threeGroupMatchesPlayed='self.group.matches-&gt;select(match | match.home = self or match.away = self)-&gt;size() = 3' noDuplicateShirtNumbers='self.players-&gt;isUnique(player | player.shirtNumber)' atLeastElevenPlayers='self.players-&gt;size() &gt;= 11'"
 * @generated
 */
public interface Team extends EObject {
	/**
	 * Returns the value of the '<em><b>Coach</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Coach#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coach</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coach</em>' containment reference.
	 * @see #setCoach(Coach)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam_Coach()
	 * @see coenmv.tdt4250.worldcup.Coach#getTeam
	 * @model opposite="team" containment="true"
	 * @generated
	 */
	Coach getCoach();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Team#getCoach <em>Coach</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coach</em>' containment reference.
	 * @see #getCoach()
	 * @generated
	 */
	void setCoach(Coach value);

	/**
	 * Returns the value of the '<em><b>Group</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Group#getTeams <em>Teams</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' container reference.
	 * @see #setGroup(Group)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam_Group()
	 * @see coenmv.tdt4250.worldcup.Group#getTeams
	 * @model opposite="teams" transient="false"
	 * @generated
	 */
	Group getGroup();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Team#getGroup <em>Group</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' container reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(Group value);

	/**
	 * Returns the value of the '<em><b>Group Stats</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.GroupStats#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Stats</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Stats</em>' containment reference.
	 * @see #setGroupStats(GroupStats)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam_GroupStats()
	 * @see coenmv.tdt4250.worldcup.GroupStats#getTeam
	 * @model opposite="team" containment="true"
	 * @generated
	 */
	GroupStats getGroupStats();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Team#getGroupStats <em>Group Stats</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Stats</em>' containment reference.
	 * @see #getGroupStats()
	 * @generated
	 */
	void setGroupStats(GroupStats value);

	/**
	 * Returns the value of the '<em><b>Country</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Country</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Country</em>' reference.
	 * @see #setCountry(Country)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam_Country()
	 * @model
	 * @generated
	 */
	Country getCountry();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Team#getCountry <em>Country</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Country</em>' reference.
	 * @see #getCountry()
	 * @generated
	 */
	void setCountry(Country value);

	/**
	 * Returns the value of the '<em><b>Players</b></em>' containment reference list.
	 * The list contents are of type {@link coenmv.tdt4250.worldcup.Player}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Players</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Players</em>' containment reference list.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getTeam_Players()
	 * @model containment="true"
	 * @generated
	 */
	EList<Player> getPlayers();

} // Team
