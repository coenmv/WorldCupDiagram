/**
 */
package coenmv.tdt4250.worldcup;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.Player#getPosition <em>Position</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.Player#getShirtNumber <em>Shirt Number</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getPlayer()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='shirtNumberWithinRange'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 shirtNumberWithinRange='self.shirtNumber &gt; 0'"
 * @generated
 */
public interface Player extends Person {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute.
	 * The literals are from the enumeration {@link coenmv.tdt4250.worldcup.EPosition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.EPosition
	 * @see #setPosition(EPosition)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getPlayer_Position()
	 * @model
	 * @generated
	 */
	EPosition getPosition();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Player#getPosition <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.EPosition
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(EPosition value);

	/**
	 * Returns the value of the '<em><b>Shirt Number</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shirt Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shirt Number</em>' attribute.
	 * @see #setShirtNumber(int)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getPlayer_ShirtNumber()
	 * @model default="1"
	 * @generated
	 */
	int getShirtNumber();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.Player#getShirtNumber <em>Shirt Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shirt Number</em>' attribute.
	 * @see #getShirtNumber()
	 * @generated
	 */
	void setShirtNumber(int value);

} // Player
