/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Stats</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getTeam <em>Team</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getMatchesPlayed <em>Matches Played</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getWon <em>Won</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getDraw <em>Draw</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getLost <em>Lost</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getGoalsFor <em>Goals For</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getGoalsAgainst <em>Goals Against</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getGoalDifference <em>Goal Difference</em>}</li>
 *   <li>{@link coenmv.tdt4250.worldcup.GroupStats#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats()
 * @model
 * @generated
 */
public interface GroupStats extends EObject {
	/**
	 * Returns the value of the '<em><b>Team</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coenmv.tdt4250.worldcup.Team#getGroupStats <em>Group Stats</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Team</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Team</em>' container reference.
	 * @see #setTeam(Team)
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_Team()
	 * @see coenmv.tdt4250.worldcup.Team#getGroupStats
	 * @model opposite="groupStats" transient="false"
	 * @generated
	 */
	Team getTeam();

	/**
	 * Sets the value of the '{@link coenmv.tdt4250.worldcup.GroupStats#getTeam <em>Team</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Team</em>' container reference.
	 * @see #getTeam()
	 * @generated
	 */
	void setTeam(Team value);

	/**
	 * Returns the value of the '<em><b>Matches Played</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matches Played</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches Played</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_MatchesPlayed()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getMatchesPlayed();

	/**
	 * Returns the value of the '<em><b>Won</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Won</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Won</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_Won()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getWon();

	/**
	 * Returns the value of the '<em><b>Draw</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Draw</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Draw</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_Draw()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getDraw();

	/**
	 * Returns the value of the '<em><b>Lost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lost</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_Lost()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getLost();

	/**
	 * Returns the value of the '<em><b>Goals For</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goals For</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goals For</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_GoalsFor()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getGoalsFor();

	/**
	 * Returns the value of the '<em><b>Goals Against</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goals Against</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goals Against</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_GoalsAgainst()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getGoalsAgainst();

	/**
	 * Returns the value of the '<em><b>Goal Difference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal Difference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal Difference</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_GoalDifference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getGoalDifference();

	/**
	 * Returns the value of the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Points</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Points</em>' attribute.
	 * @see coenmv.tdt4250.worldcup.WorldCupPackage#getGroupStats_Points()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getPoints();

} // GroupStats
