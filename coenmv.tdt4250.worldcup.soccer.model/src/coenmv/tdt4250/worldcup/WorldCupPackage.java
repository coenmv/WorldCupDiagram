/**
 */
package coenmv.tdt4250.worldcup;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see coenmv.tdt4250.worldcup.WorldCupFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface WorldCupPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "worldcup";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/coenmv.tdt4250.worldcup.soccer/model/worldCup.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "worldCup";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorldCupPackage eINSTANCE = coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl.init();

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl <em>World Cup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getWorldCup()
	 * @generated
	 */
	int WORLD_CUP = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP__YEAR = 0;

	/**
	 * The feature id for the '<em><b>Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP__HOST = 1;

	/**
	 * The feature id for the '<em><b>Champion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP__CHAMPION = 2;

	/**
	 * The feature id for the '<em><b>Rounds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP__ROUNDS = 3;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP__GROUPS = 4;

	/**
	 * The number of structural features of the '<em>World Cup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Get Round</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP___GET_ROUND__EROUNDTYPE = 0;

	/**
	 * The operation id for the '<em>Get Group</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP___GET_GROUP__EGROUPTYPE = 1;

	/**
	 * The number of operations of the '<em>World Cup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_CUP_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.CountriesImpl <em>Countries</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.CountriesImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCountries()
	 * @generated
	 */
	int COUNTRIES = 1;

	/**
	 * The feature id for the '<em><b>Countries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRIES__COUNTRIES = 0;

	/**
	 * The number of structural features of the '<em>Countries</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRIES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Countries</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRIES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.CountryImpl <em>Country</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.CountryImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCountry()
	 * @generated
	 */
	int COUNTRY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRY__NAME = 0;

	/**
	 * The number of structural features of the '<em>Country</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Country</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.HostImpl <em>Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.HostImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getHost()
	 * @generated
	 */
	int HOST = 3;

	/**
	 * The feature id for the '<em><b>Country</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOST__COUNTRY = 0;

	/**
	 * The feature id for the '<em><b>Cities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOST__CITIES = 1;

	/**
	 * The number of structural features of the '<em>Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.StadiumImpl <em>Stadium</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.StadiumImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getStadium()
	 * @generated
	 */
	int STADIUM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STADIUM__NAME = 0;

	/**
	 * The number of structural features of the '<em>Stadium</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STADIUM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Stadium</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STADIUM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.CityImpl <em>City</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.CityImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCity()
	 * @generated
	 */
	int CITY = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CITY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Stadiums</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CITY__STADIUMS = 1;

	/**
	 * The number of structural features of the '<em>City</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CITY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>City</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.GroupImpl <em>Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.GroupImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroup()
	 * @generated
	 */
	int GROUP = 6;

	/**
	 * The feature id for the '<em><b>Group Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__GROUP_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Matches</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__MATCHES = 1;

	/**
	 * The feature id for the '<em><b>Teams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__TEAMS = 2;

	/**
	 * The number of structural features of the '<em>Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Best Team</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP___GET_BEST_TEAM = 0;

	/**
	 * The operation id for the '<em>Get Second Best Team</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP___GET_SECOND_BEST_TEAM = 1;

	/**
	 * The number of operations of the '<em>Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.RoundImpl <em>Round</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.RoundImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getRound()
	 * @generated
	 */
	int ROUND = 7;

	/**
	 * The feature id for the '<em><b>Matches</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND__MATCHES = 0;

	/**
	 * The feature id for the '<em><b>Round Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND__ROUND_TYPE = 1;

	/**
	 * The feature id for the '<em><b>World Cup</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND__WORLD_CUP = 2;

	/**
	 * The number of structural features of the '<em>Round</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Next Round</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND___GET_NEXT_ROUND = 0;

	/**
	 * The number of operations of the '<em>Round</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.MatchImpl <em>Match</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.MatchImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getMatch()
	 * @generated
	 */
	int MATCH = 8;

	/**
	 * The feature id for the '<em><b>Stadium</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__STADIUM = 0;

	/**
	 * The feature id for the '<em><b>Home</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__HOME = 1;

	/**
	 * The feature id for the '<em><b>Away</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__AWAY = 2;

	/**
	 * The feature id for the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__SCORE = 3;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__TIME = 4;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__DATE = 5;

	/**
	 * The number of structural features of the '<em>Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Get Winner</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH___GET_WINNER = 0;

	/**
	 * The operation id for the '<em>Get Loser</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH___GET_LOSER = 1;

	/**
	 * The operation id for the '<em>Is Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH___IS_DRAW = 2;

	/**
	 * The number of operations of the '<em>Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.GroupMatchImpl <em>Group Match</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.GroupMatchImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroupMatch()
	 * @generated
	 */
	int GROUP_MATCH = 9;

	/**
	 * The feature id for the '<em><b>Stadium</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__STADIUM = MATCH__STADIUM;

	/**
	 * The feature id for the '<em><b>Home</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__HOME = MATCH__HOME;

	/**
	 * The feature id for the '<em><b>Away</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__AWAY = MATCH__AWAY;

	/**
	 * The feature id for the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__SCORE = MATCH__SCORE;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__TIME = MATCH__TIME;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH__DATE = MATCH__DATE;

	/**
	 * The number of structural features of the '<em>Group Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH_FEATURE_COUNT = MATCH_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Winner</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH___GET_WINNER = MATCH___GET_WINNER;

	/**
	 * The operation id for the '<em>Get Loser</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH___GET_LOSER = MATCH___GET_LOSER;

	/**
	 * The operation id for the '<em>Is Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH___IS_DRAW = MATCH___IS_DRAW;

	/**
	 * The number of operations of the '<em>Group Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_MATCH_OPERATION_COUNT = MATCH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl <em>Final Match</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.FinalMatchImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getFinalMatch()
	 * @generated
	 */
	int FINAL_MATCH = 10;

	/**
	 * The feature id for the '<em><b>Stadium</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__STADIUM = MATCH__STADIUM;

	/**
	 * The feature id for the '<em><b>Home</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__HOME = MATCH__HOME;

	/**
	 * The feature id for the '<em><b>Away</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__AWAY = MATCH__AWAY;

	/**
	 * The feature id for the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__SCORE = MATCH__SCORE;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__TIME = MATCH__TIME;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__DATE = MATCH__DATE;

	/**
	 * The feature id for the '<em><b>Used Extra Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__USED_EXTRA_TIME = MATCH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Used Penalties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__USED_PENALTIES = MATCH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Penalty Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__PENALTY_SCORE = MATCH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Round</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH__ROUND = MATCH_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Final Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH_FEATURE_COUNT = MATCH_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Winner</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH___GET_WINNER = MATCH___GET_WINNER;

	/**
	 * The operation id for the '<em>Get Loser</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH___GET_LOSER = MATCH___GET_LOSER;

	/**
	 * The operation id for the '<em>Is Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH___IS_DRAW = MATCH___IS_DRAW;

	/**
	 * The operation id for the '<em>Get Following Match</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH___GET_FOLLOWING_MATCH = MATCH_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Final Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_MATCH_OPERATION_COUNT = MATCH_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.TeamImpl <em>Team</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.TeamImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getTeam()
	 * @generated
	 */
	int TEAM = 11;

	/**
	 * The feature id for the '<em><b>Coach</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__COACH = 0;

	/**
	 * The feature id for the '<em><b>Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__GROUP = 1;

	/**
	 * The feature id for the '<em><b>Group Stats</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__GROUP_STATS = 2;

	/**
	 * The feature id for the '<em><b>Country</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__COUNTRY = 3;

	/**
	 * The feature id for the '<em><b>Players</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__PLAYERS = 4;

	/**
	 * The number of structural features of the '<em>Team</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Team</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl <em>Group Stats</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.GroupStatsImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroupStats()
	 * @generated
	 */
	int GROUP_STATS = 12;

	/**
	 * The feature id for the '<em><b>Team</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__TEAM = 0;

	/**
	 * The feature id for the '<em><b>Matches Played</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__MATCHES_PLAYED = 1;

	/**
	 * The feature id for the '<em><b>Won</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__WON = 2;

	/**
	 * The feature id for the '<em><b>Draw</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__DRAW = 3;

	/**
	 * The feature id for the '<em><b>Lost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__LOST = 4;

	/**
	 * The feature id for the '<em><b>Goals For</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__GOALS_FOR = 5;

	/**
	 * The feature id for the '<em><b>Goals Against</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__GOALS_AGAINST = 6;

	/**
	 * The feature id for the '<em><b>Goal Difference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__GOAL_DIFFERENCE = 7;

	/**
	 * The feature id for the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS__POINTS = 8;

	/**
	 * The number of structural features of the '<em>Group Stats</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Group Stats</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_STATS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.PersonImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 15;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__FIRST_NAME = 0;

	/**
	 * The feature id for the '<em><b>Surname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__SURNAME = 1;

	/**
	 * The feature id for the '<em><b>Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__FULL_NAME = 2;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.PlayerImpl <em>Player</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.PlayerImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getPlayer()
	 * @generated
	 */
	int PLAYER = 13;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__FIRST_NAME = PERSON__FIRST_NAME;

	/**
	 * The feature id for the '<em><b>Surname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__SURNAME = PERSON__SURNAME;

	/**
	 * The feature id for the '<em><b>Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__FULL_NAME = PERSON__FULL_NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__POSITION = PERSON_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Shirt Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__SHIRT_NUMBER = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER_FEATURE_COUNT = PERSON_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.impl.CoachImpl <em>Coach</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.impl.CoachImpl
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCoach()
	 * @generated
	 */
	int COACH = 14;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH__FIRST_NAME = PERSON__FIRST_NAME;

	/**
	 * The feature id for the '<em><b>Surname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH__SURNAME = PERSON__SURNAME;

	/**
	 * The feature id for the '<em><b>Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH__FULL_NAME = PERSON__FULL_NAME;

	/**
	 * The feature id for the '<em><b>Team</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH__TEAM = PERSON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Coach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH_FEATURE_COUNT = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Coach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COACH_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.EGroupType <em>EGroup Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.EGroupType
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEGroupType()
	 * @generated
	 */
	int EGROUP_TYPE = 16;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.ERoundType <em>ERound Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.ERoundType
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getERoundType()
	 * @generated
	 */
	int EROUND_TYPE = 17;

	/**
	 * The meta object id for the '{@link coenmv.tdt4250.worldcup.EPosition <em>EPosition</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.EPosition
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEPosition()
	 * @generated
	 */
	int EPOSITION = 18;

	/**
	 * The meta object id for the '<em>EDate</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.util.Date
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEDate()
	 * @generated
	 */
	int EDATE = 19;

	/**
	 * The meta object id for the '<em>EScore</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.util.Score
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEScore()
	 * @generated
	 */
	int ESCORE = 20;

	/**
	 * The meta object id for the '<em>ETime</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coenmv.tdt4250.worldcup.util.Time
	 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getETime()
	 * @generated
	 */
	int ETIME = 21;


	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.WorldCup <em>World Cup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World Cup</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup
	 * @generated
	 */
	EClass getWorldCup();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.WorldCup#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getYear()
	 * @see #getWorldCup()
	 * @generated
	 */
	EAttribute getWorldCup_Year();

	/**
	 * Returns the meta object for the containment reference '{@link coenmv.tdt4250.worldcup.WorldCup#getHost <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Host</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getHost()
	 * @see #getWorldCup()
	 * @generated
	 */
	EReference getWorldCup_Host();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.WorldCup#getChampion <em>Champion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Champion</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getChampion()
	 * @see #getWorldCup()
	 * @generated
	 */
	EReference getWorldCup_Champion();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.WorldCup#getRounds <em>Rounds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rounds</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getRounds()
	 * @see #getWorldCup()
	 * @generated
	 */
	EReference getWorldCup_Rounds();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.WorldCup#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groups</em>'.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getGroups()
	 * @see #getWorldCup()
	 * @generated
	 */
	EReference getWorldCup_Groups();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.WorldCup#getRound(coenmv.tdt4250.worldcup.ERoundType) <em>Get Round</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Round</em>' operation.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getRound(coenmv.tdt4250.worldcup.ERoundType)
	 * @generated
	 */
	EOperation getWorldCup__GetRound__ERoundType();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.WorldCup#getGroup(coenmv.tdt4250.worldcup.EGroupType) <em>Get Group</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Group</em>' operation.
	 * @see coenmv.tdt4250.worldcup.WorldCup#getGroup(coenmv.tdt4250.worldcup.EGroupType)
	 * @generated
	 */
	EOperation getWorldCup__GetGroup__EGroupType();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Countries <em>Countries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Countries</em>'.
	 * @see coenmv.tdt4250.worldcup.Countries
	 * @generated
	 */
	EClass getCountries();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Countries#getCountries <em>Countries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Countries</em>'.
	 * @see coenmv.tdt4250.worldcup.Countries#getCountries()
	 * @see #getCountries()
	 * @generated
	 */
	EReference getCountries_Countries();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Country <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Country</em>'.
	 * @see coenmv.tdt4250.worldcup.Country
	 * @generated
	 */
	EClass getCountry();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Country#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coenmv.tdt4250.worldcup.Country#getName()
	 * @see #getCountry()
	 * @generated
	 */
	EAttribute getCountry_Name();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Host <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Host</em>'.
	 * @see coenmv.tdt4250.worldcup.Host
	 * @generated
	 */
	EClass getHost();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.Host#getCountry <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Country</em>'.
	 * @see coenmv.tdt4250.worldcup.Host#getCountry()
	 * @see #getHost()
	 * @generated
	 */
	EReference getHost_Country();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Host#getCities <em>Cities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cities</em>'.
	 * @see coenmv.tdt4250.worldcup.Host#getCities()
	 * @see #getHost()
	 * @generated
	 */
	EReference getHost_Cities();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Stadium <em>Stadium</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stadium</em>'.
	 * @see coenmv.tdt4250.worldcup.Stadium
	 * @generated
	 */
	EClass getStadium();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Stadium#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coenmv.tdt4250.worldcup.Stadium#getName()
	 * @see #getStadium()
	 * @generated
	 */
	EAttribute getStadium_Name();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.City <em>City</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>City</em>'.
	 * @see coenmv.tdt4250.worldcup.City
	 * @generated
	 */
	EClass getCity();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.City#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coenmv.tdt4250.worldcup.City#getName()
	 * @see #getCity()
	 * @generated
	 */
	EAttribute getCity_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.City#getStadiums <em>Stadiums</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Stadiums</em>'.
	 * @see coenmv.tdt4250.worldcup.City#getStadiums()
	 * @see #getCity()
	 * @generated
	 */
	EReference getCity_Stadiums();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Group <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group</em>'.
	 * @see coenmv.tdt4250.worldcup.Group
	 * @generated
	 */
	EClass getGroup();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Group#getGroupType <em>Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Type</em>'.
	 * @see coenmv.tdt4250.worldcup.Group#getGroupType()
	 * @see #getGroup()
	 * @generated
	 */
	EAttribute getGroup_GroupType();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Group#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Matches</em>'.
	 * @see coenmv.tdt4250.worldcup.Group#getMatches()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Matches();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Group#getTeams <em>Teams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Teams</em>'.
	 * @see coenmv.tdt4250.worldcup.Group#getTeams()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Teams();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Group#getBestTeam() <em>Get Best Team</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Best Team</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Group#getBestTeam()
	 * @generated
	 */
	EOperation getGroup__GetBestTeam();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Group#getSecondBestTeam() <em>Get Second Best Team</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Second Best Team</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Group#getSecondBestTeam()
	 * @generated
	 */
	EOperation getGroup__GetSecondBestTeam();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Round <em>Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Round</em>'.
	 * @see coenmv.tdt4250.worldcup.Round
	 * @generated
	 */
	EClass getRound();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Round#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Matches</em>'.
	 * @see coenmv.tdt4250.worldcup.Round#getMatches()
	 * @see #getRound()
	 * @generated
	 */
	EReference getRound_Matches();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Round#getRoundType <em>Round Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Round Type</em>'.
	 * @see coenmv.tdt4250.worldcup.Round#getRoundType()
	 * @see #getRound()
	 * @generated
	 */
	EAttribute getRound_RoundType();

	/**
	 * Returns the meta object for the container reference '{@link coenmv.tdt4250.worldcup.Round#getWorldCup <em>World Cup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>World Cup</em>'.
	 * @see coenmv.tdt4250.worldcup.Round#getWorldCup()
	 * @see #getRound()
	 * @generated
	 */
	EReference getRound_WorldCup();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Round#getNextRound() <em>Get Next Round</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Next Round</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Round#getNextRound()
	 * @generated
	 */
	EOperation getRound__GetNextRound();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Match <em>Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Match</em>'.
	 * @see coenmv.tdt4250.worldcup.Match
	 * @generated
	 */
	EClass getMatch();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.Match#getStadium <em>Stadium</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stadium</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getStadium()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Stadium();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.Match#getHome <em>Home</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Home</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getHome()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Home();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.Match#getAway <em>Away</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Away</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getAway()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Away();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Match#getScore <em>Score</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Score</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getScore()
	 * @see #getMatch()
	 * @generated
	 */
	EAttribute getMatch_Score();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Match#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getTime()
	 * @see #getMatch()
	 * @generated
	 */
	EAttribute getMatch_Time();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Match#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see coenmv.tdt4250.worldcup.Match#getDate()
	 * @see #getMatch()
	 * @generated
	 */
	EAttribute getMatch_Date();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Match#getWinner() <em>Get Winner</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Winner</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Match#getWinner()
	 * @generated
	 */
	EOperation getMatch__GetWinner();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Match#getLoser() <em>Get Loser</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Loser</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Match#getLoser()
	 * @generated
	 */
	EOperation getMatch__GetLoser();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.Match#isDraw() <em>Is Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Draw</em>' operation.
	 * @see coenmv.tdt4250.worldcup.Match#isDraw()
	 * @generated
	 */
	EOperation getMatch__IsDraw();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.GroupMatch <em>Group Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Match</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupMatch
	 * @generated
	 */
	EClass getGroupMatch();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.FinalMatch <em>Final Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Final Match</em>'.
	 * @see coenmv.tdt4250.worldcup.FinalMatch
	 * @generated
	 */
	EClass getFinalMatch();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.FinalMatch#isUsedExtraTime <em>Used Extra Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Used Extra Time</em>'.
	 * @see coenmv.tdt4250.worldcup.FinalMatch#isUsedExtraTime()
	 * @see #getFinalMatch()
	 * @generated
	 */
	EAttribute getFinalMatch_UsedExtraTime();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.FinalMatch#isUsedPenalties <em>Used Penalties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Used Penalties</em>'.
	 * @see coenmv.tdt4250.worldcup.FinalMatch#isUsedPenalties()
	 * @see #getFinalMatch()
	 * @generated
	 */
	EAttribute getFinalMatch_UsedPenalties();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.FinalMatch#getPenaltyScore <em>Penalty Score</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Penalty Score</em>'.
	 * @see coenmv.tdt4250.worldcup.FinalMatch#getPenaltyScore()
	 * @see #getFinalMatch()
	 * @generated
	 */
	EAttribute getFinalMatch_PenaltyScore();

	/**
	 * Returns the meta object for the container reference '{@link coenmv.tdt4250.worldcup.FinalMatch#getRound <em>Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Round</em>'.
	 * @see coenmv.tdt4250.worldcup.FinalMatch#getRound()
	 * @see #getFinalMatch()
	 * @generated
	 */
	EReference getFinalMatch_Round();

	/**
	 * Returns the meta object for the '{@link coenmv.tdt4250.worldcup.FinalMatch#getFollowingMatch() <em>Get Following Match</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Following Match</em>' operation.
	 * @see coenmv.tdt4250.worldcup.FinalMatch#getFollowingMatch()
	 * @generated
	 */
	EOperation getFinalMatch__GetFollowingMatch();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Team <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Team</em>'.
	 * @see coenmv.tdt4250.worldcup.Team
	 * @generated
	 */
	EClass getTeam();

	/**
	 * Returns the meta object for the containment reference '{@link coenmv.tdt4250.worldcup.Team#getCoach <em>Coach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Coach</em>'.
	 * @see coenmv.tdt4250.worldcup.Team#getCoach()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Coach();

	/**
	 * Returns the meta object for the container reference '{@link coenmv.tdt4250.worldcup.Team#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Group</em>'.
	 * @see coenmv.tdt4250.worldcup.Team#getGroup()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Group();

	/**
	 * Returns the meta object for the containment reference '{@link coenmv.tdt4250.worldcup.Team#getGroupStats <em>Group Stats</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Stats</em>'.
	 * @see coenmv.tdt4250.worldcup.Team#getGroupStats()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_GroupStats();

	/**
	 * Returns the meta object for the reference '{@link coenmv.tdt4250.worldcup.Team#getCountry <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Country</em>'.
	 * @see coenmv.tdt4250.worldcup.Team#getCountry()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Country();

	/**
	 * Returns the meta object for the containment reference list '{@link coenmv.tdt4250.worldcup.Team#getPlayers <em>Players</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Players</em>'.
	 * @see coenmv.tdt4250.worldcup.Team#getPlayers()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Players();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.GroupStats <em>Group Stats</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Stats</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats
	 * @generated
	 */
	EClass getGroupStats();

	/**
	 * Returns the meta object for the container reference '{@link coenmv.tdt4250.worldcup.GroupStats#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Team</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getTeam()
	 * @see #getGroupStats()
	 * @generated
	 */
	EReference getGroupStats_Team();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getMatchesPlayed <em>Matches Played</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matches Played</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getMatchesPlayed()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_MatchesPlayed();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getWon <em>Won</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Won</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getWon()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_Won();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getDraw <em>Draw</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Draw</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getDraw()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_Draw();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getLost <em>Lost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lost</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getLost()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_Lost();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getGoalsFor <em>Goals For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Goals For</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getGoalsFor()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_GoalsFor();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getGoalsAgainst <em>Goals Against</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Goals Against</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getGoalsAgainst()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_GoalsAgainst();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getGoalDifference <em>Goal Difference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Goal Difference</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getGoalDifference()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_GoalDifference();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.GroupStats#getPoints <em>Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Points</em>'.
	 * @see coenmv.tdt4250.worldcup.GroupStats#getPoints()
	 * @see #getGroupStats()
	 * @generated
	 */
	EAttribute getGroupStats_Points();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Player <em>Player</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Player</em>'.
	 * @see coenmv.tdt4250.worldcup.Player
	 * @generated
	 */
	EClass getPlayer();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Player#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position</em>'.
	 * @see coenmv.tdt4250.worldcup.Player#getPosition()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Position();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Player#getShirtNumber <em>Shirt Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shirt Number</em>'.
	 * @see coenmv.tdt4250.worldcup.Player#getShirtNumber()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_ShirtNumber();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Coach <em>Coach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coach</em>'.
	 * @see coenmv.tdt4250.worldcup.Coach
	 * @generated
	 */
	EClass getCoach();

	/**
	 * Returns the meta object for the container reference '{@link coenmv.tdt4250.worldcup.Coach#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Team</em>'.
	 * @see coenmv.tdt4250.worldcup.Coach#getTeam()
	 * @see #getCoach()
	 * @generated
	 */
	EReference getCoach_Team();

	/**
	 * Returns the meta object for class '{@link coenmv.tdt4250.worldcup.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see coenmv.tdt4250.worldcup.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Person#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see coenmv.tdt4250.worldcup.Person#getFirstName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Person#getSurname <em>Surname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Surname</em>'.
	 * @see coenmv.tdt4250.worldcup.Person#getSurname()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Surname();

	/**
	 * Returns the meta object for the attribute '{@link coenmv.tdt4250.worldcup.Person#getFullName <em>Full Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Full Name</em>'.
	 * @see coenmv.tdt4250.worldcup.Person#getFullName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_FullName();

	/**
	 * Returns the meta object for enum '{@link coenmv.tdt4250.worldcup.EGroupType <em>EGroup Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EGroup Type</em>'.
	 * @see coenmv.tdt4250.worldcup.EGroupType
	 * @generated
	 */
	EEnum getEGroupType();

	/**
	 * Returns the meta object for enum '{@link coenmv.tdt4250.worldcup.ERoundType <em>ERound Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ERound Type</em>'.
	 * @see coenmv.tdt4250.worldcup.ERoundType
	 * @generated
	 */
	EEnum getERoundType();

	/**
	 * Returns the meta object for enum '{@link coenmv.tdt4250.worldcup.EPosition <em>EPosition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EPosition</em>'.
	 * @see coenmv.tdt4250.worldcup.EPosition
	 * @generated
	 */
	EEnum getEPosition();

	/**
	 * Returns the meta object for data type '{@link coenmv.tdt4250.worldcup.util.Date <em>EDate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EDate</em>'.
	 * @see coenmv.tdt4250.worldcup.util.Date
	 * @model instanceClass="coenmv.tdt4250.worldcup.util.Date"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='dateWithinRange'"
	 * @generated
	 */
	EDataType getEDate();

	/**
	 * Returns the meta object for data type '{@link coenmv.tdt4250.worldcup.util.Score <em>EScore</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EScore</em>'.
	 * @see coenmv.tdt4250.worldcup.util.Score
	 * @model instanceClass="coenmv.tdt4250.worldcup.util.Score"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='scoreWithinRange'"
	 * @generated
	 */
	EDataType getEScore();

	/**
	 * Returns the meta object for data type '{@link coenmv.tdt4250.worldcup.util.Time <em>ETime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ETime</em>'.
	 * @see coenmv.tdt4250.worldcup.util.Time
	 * @model instanceClass="coenmv.tdt4250.worldcup.util.Time"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='timeWithinRange'"
	 * @generated
	 */
	EDataType getETime();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorldCupFactory getWorldCupFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.WorldCupImpl <em>World Cup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getWorldCup()
		 * @generated
		 */
		EClass WORLD_CUP = eINSTANCE.getWorldCup();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORLD_CUP__YEAR = eINSTANCE.getWorldCup_Year();

		/**
		 * The meta object literal for the '<em><b>Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_CUP__HOST = eINSTANCE.getWorldCup_Host();

		/**
		 * The meta object literal for the '<em><b>Champion</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_CUP__CHAMPION = eINSTANCE.getWorldCup_Champion();

		/**
		 * The meta object literal for the '<em><b>Rounds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_CUP__ROUNDS = eINSTANCE.getWorldCup_Rounds();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_CUP__GROUPS = eINSTANCE.getWorldCup_Groups();

		/**
		 * The meta object literal for the '<em><b>Get Round</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation WORLD_CUP___GET_ROUND__EROUNDTYPE = eINSTANCE.getWorldCup__GetRound__ERoundType();

		/**
		 * The meta object literal for the '<em><b>Get Group</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation WORLD_CUP___GET_GROUP__EGROUPTYPE = eINSTANCE.getWorldCup__GetGroup__EGroupType();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.CountriesImpl <em>Countries</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.CountriesImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCountries()
		 * @generated
		 */
		EClass COUNTRIES = eINSTANCE.getCountries();

		/**
		 * The meta object literal for the '<em><b>Countries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COUNTRIES__COUNTRIES = eINSTANCE.getCountries_Countries();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.CountryImpl <em>Country</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.CountryImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCountry()
		 * @generated
		 */
		EClass COUNTRY = eINSTANCE.getCountry();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COUNTRY__NAME = eINSTANCE.getCountry_Name();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.HostImpl <em>Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.HostImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getHost()
		 * @generated
		 */
		EClass HOST = eINSTANCE.getHost();

		/**
		 * The meta object literal for the '<em><b>Country</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOST__COUNTRY = eINSTANCE.getHost_Country();

		/**
		 * The meta object literal for the '<em><b>Cities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOST__CITIES = eINSTANCE.getHost_Cities();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.StadiumImpl <em>Stadium</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.StadiumImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getStadium()
		 * @generated
		 */
		EClass STADIUM = eINSTANCE.getStadium();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STADIUM__NAME = eINSTANCE.getStadium_Name();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.CityImpl <em>City</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.CityImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCity()
		 * @generated
		 */
		EClass CITY = eINSTANCE.getCity();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CITY__NAME = eINSTANCE.getCity_Name();

		/**
		 * The meta object literal for the '<em><b>Stadiums</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CITY__STADIUMS = eINSTANCE.getCity_Stadiums();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.GroupImpl <em>Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.GroupImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroup()
		 * @generated
		 */
		EClass GROUP = eINSTANCE.getGroup();

		/**
		 * The meta object literal for the '<em><b>Group Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP__GROUP_TYPE = eINSTANCE.getGroup_GroupType();

		/**
		 * The meta object literal for the '<em><b>Matches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__MATCHES = eINSTANCE.getGroup_Matches();

		/**
		 * The meta object literal for the '<em><b>Teams</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__TEAMS = eINSTANCE.getGroup_Teams();

		/**
		 * The meta object literal for the '<em><b>Get Best Team</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUP___GET_BEST_TEAM = eINSTANCE.getGroup__GetBestTeam();

		/**
		 * The meta object literal for the '<em><b>Get Second Best Team</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUP___GET_SECOND_BEST_TEAM = eINSTANCE.getGroup__GetSecondBestTeam();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.RoundImpl <em>Round</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.RoundImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getRound()
		 * @generated
		 */
		EClass ROUND = eINSTANCE.getRound();

		/**
		 * The meta object literal for the '<em><b>Matches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROUND__MATCHES = eINSTANCE.getRound_Matches();

		/**
		 * The meta object literal for the '<em><b>Round Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROUND__ROUND_TYPE = eINSTANCE.getRound_RoundType();

		/**
		 * The meta object literal for the '<em><b>World Cup</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROUND__WORLD_CUP = eINSTANCE.getRound_WorldCup();

		/**
		 * The meta object literal for the '<em><b>Get Next Round</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROUND___GET_NEXT_ROUND = eINSTANCE.getRound__GetNextRound();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.MatchImpl <em>Match</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.MatchImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getMatch()
		 * @generated
		 */
		EClass MATCH = eINSTANCE.getMatch();

		/**
		 * The meta object literal for the '<em><b>Stadium</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__STADIUM = eINSTANCE.getMatch_Stadium();

		/**
		 * The meta object literal for the '<em><b>Home</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__HOME = eINSTANCE.getMatch_Home();

		/**
		 * The meta object literal for the '<em><b>Away</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__AWAY = eINSTANCE.getMatch_Away();

		/**
		 * The meta object literal for the '<em><b>Score</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCH__SCORE = eINSTANCE.getMatch_Score();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCH__TIME = eINSTANCE.getMatch_Time();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCH__DATE = eINSTANCE.getMatch_Date();

		/**
		 * The meta object literal for the '<em><b>Get Winner</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MATCH___GET_WINNER = eINSTANCE.getMatch__GetWinner();

		/**
		 * The meta object literal for the '<em><b>Get Loser</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MATCH___GET_LOSER = eINSTANCE.getMatch__GetLoser();

		/**
		 * The meta object literal for the '<em><b>Is Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MATCH___IS_DRAW = eINSTANCE.getMatch__IsDraw();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.GroupMatchImpl <em>Group Match</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.GroupMatchImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroupMatch()
		 * @generated
		 */
		EClass GROUP_MATCH = eINSTANCE.getGroupMatch();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.FinalMatchImpl <em>Final Match</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.FinalMatchImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getFinalMatch()
		 * @generated
		 */
		EClass FINAL_MATCH = eINSTANCE.getFinalMatch();

		/**
		 * The meta object literal for the '<em><b>Used Extra Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FINAL_MATCH__USED_EXTRA_TIME = eINSTANCE.getFinalMatch_UsedExtraTime();

		/**
		 * The meta object literal for the '<em><b>Used Penalties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FINAL_MATCH__USED_PENALTIES = eINSTANCE.getFinalMatch_UsedPenalties();

		/**
		 * The meta object literal for the '<em><b>Penalty Score</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FINAL_MATCH__PENALTY_SCORE = eINSTANCE.getFinalMatch_PenaltyScore();

		/**
		 * The meta object literal for the '<em><b>Round</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FINAL_MATCH__ROUND = eINSTANCE.getFinalMatch_Round();

		/**
		 * The meta object literal for the '<em><b>Get Following Match</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FINAL_MATCH___GET_FOLLOWING_MATCH = eINSTANCE.getFinalMatch__GetFollowingMatch();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.TeamImpl <em>Team</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.TeamImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getTeam()
		 * @generated
		 */
		EClass TEAM = eINSTANCE.getTeam();

		/**
		 * The meta object literal for the '<em><b>Coach</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__COACH = eINSTANCE.getTeam_Coach();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__GROUP = eINSTANCE.getTeam_Group();

		/**
		 * The meta object literal for the '<em><b>Group Stats</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__GROUP_STATS = eINSTANCE.getTeam_GroupStats();

		/**
		 * The meta object literal for the '<em><b>Country</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__COUNTRY = eINSTANCE.getTeam_Country();

		/**
		 * The meta object literal for the '<em><b>Players</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__PLAYERS = eINSTANCE.getTeam_Players();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.GroupStatsImpl <em>Group Stats</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.GroupStatsImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getGroupStats()
		 * @generated
		 */
		EClass GROUP_STATS = eINSTANCE.getGroupStats();

		/**
		 * The meta object literal for the '<em><b>Team</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP_STATS__TEAM = eINSTANCE.getGroupStats_Team();

		/**
		 * The meta object literal for the '<em><b>Matches Played</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__MATCHES_PLAYED = eINSTANCE.getGroupStats_MatchesPlayed();

		/**
		 * The meta object literal for the '<em><b>Won</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__WON = eINSTANCE.getGroupStats_Won();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__DRAW = eINSTANCE.getGroupStats_Draw();

		/**
		 * The meta object literal for the '<em><b>Lost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__LOST = eINSTANCE.getGroupStats_Lost();

		/**
		 * The meta object literal for the '<em><b>Goals For</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__GOALS_FOR = eINSTANCE.getGroupStats_GoalsFor();

		/**
		 * The meta object literal for the '<em><b>Goals Against</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__GOALS_AGAINST = eINSTANCE.getGroupStats_GoalsAgainst();

		/**
		 * The meta object literal for the '<em><b>Goal Difference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__GOAL_DIFFERENCE = eINSTANCE.getGroupStats_GoalDifference();

		/**
		 * The meta object literal for the '<em><b>Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_STATS__POINTS = eINSTANCE.getGroupStats_Points();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.PlayerImpl <em>Player</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.PlayerImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getPlayer()
		 * @generated
		 */
		EClass PLAYER = eINSTANCE.getPlayer();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__POSITION = eINSTANCE.getPlayer_Position();

		/**
		 * The meta object literal for the '<em><b>Shirt Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__SHIRT_NUMBER = eINSTANCE.getPlayer_ShirtNumber();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.CoachImpl <em>Coach</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.CoachImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getCoach()
		 * @generated
		 */
		EClass COACH = eINSTANCE.getCoach();

		/**
		 * The meta object literal for the '<em><b>Team</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COACH__TEAM = eINSTANCE.getCoach_Team();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.impl.PersonImpl
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__FIRST_NAME = eINSTANCE.getPerson_FirstName();

		/**
		 * The meta object literal for the '<em><b>Surname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__SURNAME = eINSTANCE.getPerson_Surname();

		/**
		 * The meta object literal for the '<em><b>Full Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__FULL_NAME = eINSTANCE.getPerson_FullName();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.EGroupType <em>EGroup Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.EGroupType
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEGroupType()
		 * @generated
		 */
		EEnum EGROUP_TYPE = eINSTANCE.getEGroupType();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.ERoundType <em>ERound Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.ERoundType
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getERoundType()
		 * @generated
		 */
		EEnum EROUND_TYPE = eINSTANCE.getERoundType();

		/**
		 * The meta object literal for the '{@link coenmv.tdt4250.worldcup.EPosition <em>EPosition</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.EPosition
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEPosition()
		 * @generated
		 */
		EEnum EPOSITION = eINSTANCE.getEPosition();

		/**
		 * The meta object literal for the '<em>EDate</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.util.Date
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEDate()
		 * @generated
		 */
		EDataType EDATE = eINSTANCE.getEDate();

		/**
		 * The meta object literal for the '<em>EScore</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.util.Score
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getEScore()
		 * @generated
		 */
		EDataType ESCORE = eINSTANCE.getEScore();

		/**
		 * The meta object literal for the '<em>ETime</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coenmv.tdt4250.worldcup.util.Time
		 * @see coenmv.tdt4250.worldcup.impl.WorldCupPackageImpl#getETime()
		 * @generated
		 */
		EDataType ETIME = eINSTANCE.getETime();

	}

} //WorldCupPackage
