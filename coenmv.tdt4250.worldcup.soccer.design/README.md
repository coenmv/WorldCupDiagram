# Design

This bundle contains the Sirius .odesign file which Sirius uses to represent the diagram models. The .odesign file can be found in the `description` directory. Furthermore, some diagrams use images / icons which are contained in the `images` directory (containing country flag images) and its child directory `icons`. In the source is an auto-generated `Activator.java` file (implementing the Sirius activator) and a `Services.java` file which is used by some diagram elements to do calculations that are to complicated to directly insert into the .odesign file.

To make your own worldcup diagrams, refer to the [project README](https://gitlab.stud.idi.ntnu.no/coenmv/WorldCupDiagram/blob/master/README.md).