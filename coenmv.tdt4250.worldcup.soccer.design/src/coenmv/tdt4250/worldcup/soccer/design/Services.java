package coenmv.tdt4250.worldcup.soccer.design;

import java.util.ArrayList;
import java.util.Collection;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;

import coenmv.tdt4250.worldcup.*;
import coenmv.tdt4250.worldcup.util.DateComparator;
import coenmv.tdt4250.worldcup.util.TeamComparator;

/**
 * The services class used by VSM.
 */
public class Services {

	public Collection<Team> availableTeams(Round round) {
		if (round.getRoundType() == ERoundType.ROUND_OF_16) {
			Collection<Team> teams = availableRoundOf16HomeTeams(round);
			teams.addAll(availableRoundOf16AwayTeams(round));
			return teams;
		} else if (round.getRoundType() == ERoundType.QUARTERFINALS) {
			return availableQuarterFinalsTeams(round);
		} else if (round.getRoundType() == ERoundType.SEMIFINALS) {
			return availableSemiFinalsTeams(round);
		} else if (round.getRoundType() == ERoundType.THIRD_PLACE_PLAYOFF) {
			return availableThirdPlacePlayoffTeams(round);
		} else { // round.getRoundType() == ERoundType.FINAL
			return availableFinalTeams(round);
		}
	}

	private Collection<Team> availableQuarterFinalsTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.QUARTERFINALS) {
			return teams;
		}
		EList<FinalMatch> quarterFinalsMatches = round.getMatches();
		List<Team> quarterFinalsTeams = new ArrayList<Team>();
		for (FinalMatch match : quarterFinalsMatches) {
			quarterFinalsTeams.add(match.getHome());
			quarterFinalsTeams.add(match.getAway());
		}
		EList<FinalMatch> roundOf16Matches = round.getWorldCup().getRound(ERoundType.ROUND_OF_16).getMatches();
		List<Team> winnersRoundOf16 = new ArrayList<Team>();
		for (FinalMatch match : roundOf16Matches) {
			Team winner = match.getWinner();
			if (winner != null) {
				winnersRoundOf16.add(winner);
			}
		}
		for (Team team : winnersRoundOf16) {
			if (!quarterFinalsTeams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	private Collection<Team> availableSemiFinalsTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.SEMIFINALS) {
			return teams;
		}
		EList<FinalMatch> semiFinalsMatches = round.getMatches();
		List<Team> semiFinalsTeams = new ArrayList<Team>();
		for (FinalMatch match : semiFinalsMatches) {
			semiFinalsTeams.add(match.getHome());
			semiFinalsTeams.add(match.getAway());
		}
		EList<FinalMatch> quarterFinalsMatches = round.getWorldCup().getRound(ERoundType.QUARTERFINALS).getMatches();
		List<Team> winnersQuarterFinals = new ArrayList<Team>();
		for (FinalMatch match : quarterFinalsMatches) {
			Team winner = match.getWinner();
			if (winner != null) {
				winnersQuarterFinals.add(winner);
			}
		}
		for (Team team : winnersQuarterFinals) {
			if (!semiFinalsTeams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	private Collection<Team> availableThirdPlacePlayoffTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.THIRD_PLACE_PLAYOFF) {
			return teams;
		}
		EList<FinalMatch> thirdPlacePlayoffMatch = round.getMatches();
		List<Team> thirdPlacePlayoffTeams = new ArrayList<Team>();
		for (FinalMatch match : thirdPlacePlayoffMatch) {
			thirdPlacePlayoffTeams.add(match.getHome());
			thirdPlacePlayoffTeams.add(match.getAway());
		}
		EList<FinalMatch> semiFinalsMatches = round.getWorldCup().getRound(ERoundType.SEMIFINALS).getMatches();
		List<Team> losersSemiFinals = new ArrayList<Team>();
		for (FinalMatch match : semiFinalsMatches) {
			Team loser = match.getLoser();
			if (loser != null) {
				losersSemiFinals.add(loser);
			}
		}
		for (Team team : losersSemiFinals) {
			if (!thirdPlacePlayoffTeams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	private Collection<Team> availableFinalTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.FINAL) {
			return teams;
		}
		EList<FinalMatch> finalMatch = round.getMatches();
		List<Team> finalTeams = new ArrayList<Team>();
		for (FinalMatch match : finalMatch) {
			finalTeams.add(match.getHome());
			finalTeams.add(match.getAway());
		}
		EList<FinalMatch> semiFinalsMatches = round.getWorldCup().getRound(ERoundType.SEMIFINALS).getMatches();
		List<Team> winnersSemiFinals = new ArrayList<Team>();
		for (FinalMatch match : semiFinalsMatches) {
			Team winner = match.getWinner();
			if (winner != null) {
				winnersSemiFinals.add(winner);
			}
		}
		for (Team team : winnersSemiFinals) {
			if (!finalTeams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	public Collection<Team> availableRoundOf16AwayTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.ROUND_OF_16) {
			return teams;
		}
		EList<FinalMatch> roundOf16Matches = round.getMatches();
		List<Team> roundOf16Teams = new ArrayList<Team>();
		for (FinalMatch match : roundOf16Matches) {
			roundOf16Teams.add(match.getHome());
			roundOf16Teams.add(match.getAway());
		}
		EList<Group> groups = round.getWorldCup().getGroups();
		List<Team> secondBestTeams = new ArrayList<Team>();
		for (Group group : groups) {
			secondBestTeams.add(group.getSecondBestTeam());
		}
		for (Team team : secondBestTeams) {
			if (!roundOf16Teams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	public Collection<Team> availableRoundOf16HomeTeams(Round round) {
		List<Team> teams = new LinkedList<Team>();
		if (round.getRoundType() != ERoundType.ROUND_OF_16) {
			return teams;
		}
		EList<FinalMatch> roundOf16Matches = round.getMatches();
		List<Team> roundOf16Teams = new ArrayList<Team>();
		for (FinalMatch match : roundOf16Matches) {
			roundOf16Teams.add(match.getHome());
			roundOf16Teams.add(match.getAway());
		}
		EList<Group> groups = round.getWorldCup().getGroups();
		List<Team> bestTeams = new ArrayList<Team>();
		for (Group group : groups) {
			bestTeams.add(group.getBestTeam());
		}
		for (Team team : bestTeams) {
			if (!roundOf16Teams.contains(team)) {
				teams.add(team);
			}
		}
		return teams;
	}

	public String groupTypeToString(Group group) {
		EGroupType groupType = group.getGroupType();
		return enumToString(groupType);
	}

	public String roundTypeToString(Round round) {
		ERoundType roundType = round.getRoundType();
		return enumToString(roundType);
	}
	
	public String positionToString(Player player) {
		EPosition position = player.getPosition();
		return enumToString(position);
	}
	
	private String enumToString(Enumerator _enum) {
		String[] parts = _enum.toString().toLowerCase().split("_");
		for (int i = 0; i < parts.length; i++) {
			parts[i] = parts[i].substring(0, 1).toUpperCase() + parts[i].substring(1);
		}
		return String.join(" ", parts);
	}
	
	public String stringToEnumString(String s) {
		String[] parts = s.toLowerCase().split(" ");
		for (int i = 0; i < parts.length; i++) {
			parts[i] = parts[i].toUpperCase();
		}
		return String.join("_", parts);
	}
	
	public EPosition positionStringToPosition(String s) {
		return EPosition.get(stringToEnumString(s));
	}
	
	public ERoundType roundTypeStringToRoundType(String s) {
		return ERoundType.get(stringToEnumString(s));
	}
	
	public EGroupType groupTypeStringToGroupType(String s) {
		return EGroupType.get(stringToEnumString(s));
	}

	public Collection<Team> sortedTeams(Group group) {
		try {
			List<Team> teams = new LinkedList<Team>(group.getTeams());
			teams.sort(new TeamComparator());
			Collections.reverse(teams);
			return teams;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Collection<Match> sortedMatches(Round round) {
		return round.getMatches().stream().sorted(new Comparator<Match>() {

			private DateComparator dateComparator = new DateComparator();

			@Override
			public int compare(Match match1, Match match2) {
				return dateComparator.compare(match1.getDate(), match2.getDate());
			}

		}).collect(Collectors.toList());
	}

	public Collection<Match> sortedMatches(Group group) {
		return group.getMatches().stream().sorted(new Comparator<Match>() {

			private DateComparator dateComparator = new DateComparator();

			@Override
			public int compare(Match match1, Match match2) {
				return dateComparator.compare(match1.getDate(), match2.getDate());
			}

		}).collect(Collectors.toList());
	}

}
