# Demo

This bundle shows how worldcup diagrams can be created in Sirius. It contains a `representations.aird` file which implements diagrams for the `WorldCup.xmi` and `Countries.xmi` file in the `coenmv.tdt4250.worldcup.main` directory in the `coenmv.tdt4250.worldcup.soccer.model` bundle. It includes all the diagrams listed in the project _README_.

Finally, there is a directory called `images` containing two images of two of the demo diagrams. They can also be found below:

##### Groups Diagram
![Groups Diagram](images/groups_diagram.png)

##### Rounds Tree Layout Diagram
![Rounds Tree Layout Diagram](images/rounds_tree_layout_diagram.png)
